
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import com.nykredit.kundeservice.swing.NTableRenderer;

public class ChartTableRenderer extends NTableRenderer {
 private static final long serialVersionUID = 1L;
 private boolean legend;

 public ChartTableRenderer(boolean legend){
	 this.legend = legend;
 }
 
 public Component getTableCellRendererComponent (JTable table,Object obj, boolean isSelected, boolean hasFocus, int row, int column) {
	 Component cell = super.getTableCellRendererComponent(table, obj, isSelected, hasFocus, row, column);
	 cell.setFont(new Font("Arial",Font.PLAIN,9));
    
	if(legend){
		  if (column > 1)
			  	setHorizontalAlignment(SwingConstants.CENTER);
			  else
				setHorizontalAlignment(SwingConstants.LEFT);
		
		  if(column == 0 & row == 0 & !table.getValueAt(row, 1).toString().contentEquals("Total"))cell.setBackground(new Color(64,105,156));  
		  if(column == 0 & row == 1 & !table.getValueAt(row, 1).toString().contentEquals("Total"))cell.setBackground(new Color(158,65,62)); 
		  if(column == 0 & row == 2 & !table.getValueAt(row, 1).toString().contentEquals("Total"))cell.setBackground(new Color(127,154,72)); 
		  if(column == 0 & row == 3 & !table.getValueAt(row, 1).toString().contentEquals("Total"))cell.setBackground(new Color(105,81,133)); 
		  if(column == 0 & row == 4 & !table.getValueAt(row, 1).toString().contentEquals("Total"))cell.setBackground(new Color(60,141,163)); 
		  if(column == 0 & row == 5 & !table.getValueAt(row, 1).toString().contentEquals("Total"))cell.setBackground(new Color(204,123,56)); 
		  if(column == 0 & row == 6 & !table.getValueAt(row, 1).toString().contentEquals("Total"))cell.setBackground(new Color(79,129,189)); 
		  if(column == 0 & row == 7 & !table.getValueAt(row, 1).toString().contentEquals("Total"))cell.setBackground(new Color(198,214,172));
		  if(column == 0 & row == 8 & !table.getValueAt(row, 1).toString().contentEquals("Total"))cell.setBackground(new Color(217,170,169)); 
		  if(column == 0 & row == 9 & !table.getValueAt(row, 1).toString().contentEquals("Total"))cell.setBackground(new Color(247,150,70)); 
		  if(column == 0 & row == 10 & !table.getValueAt(row, 1).toString().contentEquals("Total"))cell.setBackground(new Color(186,176,201)); 
		  if(column == 0 & row == 11 & !table.getValueAt(row, 1).toString().contentEquals("Total"))cell.setBackground(new Color(170,186,215)); 
		  if(column == 0 & row == 12 & !table.getValueAt(row, 1).toString().contentEquals("Total"))cell.setBackground(new Color(155,187,89)); 
		  if(column == 0 & row == 13 & !table.getValueAt(row, 1).toString().contentEquals("Total"))cell.setBackground(new Color(169,206,220)); 
		  if(column == 0 & row == 14 & !table.getValueAt(row, 1).toString().contentEquals("Total"))cell.setBackground(new Color(192,80,77)); 
		  if(column == 0 & row == 15 & !table.getValueAt(row, 1).toString().contentEquals("Total"))cell.setBackground(new Color(128,100,162));
	}else{
		  if (column > 0)
			  	setHorizontalAlignment(SwingConstants.CENTER);
			  else
				setHorizontalAlignment(SwingConstants.LEFT);
	}
  	addMarking(cell,isSelected,new Color(0,0,128),80);

  	
  
  
     return cell;
 }
}
