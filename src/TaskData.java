
public class TaskData {
	
	public enum TaskTypes{
		Hovednumre(true, false),
		S70151600(true, false),
		Boligejerneshus(true, false),
		Privatcentre(true, false),
		Forsikring(true, false),
		Hotline(true, false),
		Tekniskhotline(true, false),
		Pensionhotline(true, false),
		Stabe(true, false),
		Kunder(true, false),
		Erhverv(true, false),
		Erhvervstorkunder(true, false),
		Erhvervoverflow(true, false),
		Basisintern(true, false),
		Boligintern(true, false),
		Investeringpension(true, false),
		Kredit(true, false),
		Forsikringintern(true, false),
		Totalkredit(true, false),
		Vipservice(true, false),
		Vipcenterkald(true, false),
		Datafejl(true, false),
		Postogfax(true, false),
		Forsikringsopgaver(true, false),
		Webdesk(true, false),
		Personligekort(true, false),
		EmailIndgående(true, true),
		EmailB9(true, true),
		EmailBehandlede(true, true),
		EmailKampagne(true, true);
		
		private boolean isCall;
		private boolean isEmail;
		
		public boolean IsCall(){
			return this.isCall;
		}
		public boolean IsEmail(){
			return this.isEmail;
		}
		
		private TaskTypes(boolean isCall, boolean isEmail){
			this.isCall = isCall;
		}
	}
	
	private TaskTypes taskType;
	
	private int totalReceived;
	private int handled;
	private int handledWithinLimits;
	private int internal;
	private int exceeded;
	
	public TaskData(TaskTypes taskType, int totalReceived, int handled, int handledWithinLimits, int internal, int exceeded){
		this.taskType = taskType;
		this.totalReceived = totalReceived;
		this.handled = handled;
		this.handledWithinLimits = handledWithinLimits;
		this.internal = internal;
		this.exceeded = exceeded;
	}
	
	public TaskTypes getTaskType(){
		return this.taskType;
	}

	public int getTotalReceived(){
		return this.totalReceived;
	}

	public int getHandled(){
		return this.handled;
	}

	public int getHandledWithinLimits() {
		return this.handledWithinLimits;
	}
	
	public int getInternalCalls() {
		return this.internal;
	}
	
	public int getExceeded() {
		return this.exceeded;
	}
}