
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;



class ChartWithTableMouseListener extends MouseAdapter {
	Component component;
	
	public ChartWithTableMouseListener(Component component) {
		this.component = component;
	}

	public void mousePressed(MouseEvent e){
		if (e.isPopupTrigger())
			doPop(e);
	}
	
	public void mouseReleased(MouseEvent e){
		if (e.isPopupTrigger())
			doPop(e);
	}
	
	private void doPop(MouseEvent e){
		NTableMenu menu = new NTableMenu(component);
		menu.show(e.getComponent(), e.getX(), e.getY());
	}
	
	private class NTableMenu extends JPopupMenu {
		private static final long serialVersionUID = 1L;
		JMenuItem copyMenuItem;
		Component component;

		public NTableMenu(Component jTable){
			add(getCopyMenuItem());
			this.component = jTable;
			}

			private JMenuItem getCopyMenuItem() {
				if (copyMenuItem == null) {
					copyMenuItem = new JMenuItem();
					copyMenuItem.setText("Kopi�r");
					copyMenuItem.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							
							int width = component.getWidth();
							int height = component.getHeight();
							BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
							Graphics big = bi.getGraphics();
							component.paint(big);
							setClipboard(bi);

						}
					});
				}
				return copyMenuItem;
			}
			
			public void setClipboard(BufferedImage image) {
			    ImageSelection imgSel = new ImageSelection(image);
			    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(imgSel, null);
			}
			
		} 
	


}