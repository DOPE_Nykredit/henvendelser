
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import org.joda.time.DateTime;

import com.nykredit.kundeservice.util.Date;
import com.nykredit.kundeservice.data.CTIRConnection;

public class EOracle extends CTIRConnection{

	public EOracle(String program){
		try{
		Connect();
		}catch(SQLException e){e.printStackTrace();}
		SetupSpy(program);
	}

	  private String getStartDate(String start, char when) {
	    Date d = new Date();
	    d.override(start + " 00:00:00:000");
	    if (when == 'D') d.edit(when, -15);
	    else if (when == 'W')
	      d.edit(when, -11);
	    else if (when == 'Y') {
	      d.edit(when, -3);
	    }
	    else {
	      d.edit(when, -13);
	    }
	    return d.ToString("yyyy-MM-dd");
	  }
	
	  private String getDateDescriptionFormat(char when) {
		    if (when == 'D') return "DY' || ' ' || '\"Uge\" '|| 'IW";
		    if (when == 'W') return "\"Uge\" '||'IW";
		    if (when == 'M') return "Mon.' || ' ' || 'YYYY";
		    if (when == 'Y') return "YYYY";
		    return null;
		  }

		  private String getDateFormatString(char when) {
		    if (when == 'D') return "DD";
		    if (when == 'W') return "IW";
		    if (when == 'M') return "MM";
		    if (when == 'Y') return "YY";
		    return null;
		  }
	
	private ResultSet getTable(String sql){
		try {
			return Hent_tabel(sql);
		} catch (SQLException e) {e.printStackTrace();}
		return null;
	}
	
	public Vector<CallQueue> getCallQueueArray(String start, String slut,char c){
		Vector<CallQueue> al = new Vector<CallQueue>();
		
		String sql = 
				"SELECT " +
				" 	TO_CHAR(MIN(DATO)) AS SORT,   								" +
				"	TO_CHAR(MIN(DATO),'"+getDateDescriptionFormat(c)+"') AS P	" +
				"FROM 															" +
				"	KS_DRIFT.DATO		 										" +
				"WHERE 															" +
			    "	DATO >= '"+getStartDate(start,c)+"' AND        				" +
			    "   DATO <= '"+ slut+"' AND 									" +
			    "	TO_CHAR(DATO,'DY') NOT IN ('AB')     						" +
			    "GROUP BY														" +
			    "	TRUNC(DATO,'"+getDateFormatString(c)+"') 					" +
			    "ORDER BY 1														";
		ResultSet rs = getTable(sql);
		
		try {
			while (rs.next()){
				al.add(new CallQueue(rs.getString("P")));
			}
		} catch (SQLException e) {e.printStackTrace();}
		return al;
	}

	public Vector<CallQueue> getQueueReport(String start, String slut,char when,Vector<CallQueue> al){
		String sql =
				"SELECT 																	" +
			    "	TO_CHAR(MIN(TIDSPUNKT),'"+getDateDescriptionFormat(when)+"') AS PERIOD,	" +
			    "   NVL(QUEUE,' ') AS QUEUE, 												" +
			    "   NVL(SUM(ANTAL_KALD),0) AS CALLS, 										" +
			    "   NVL(SUM(ANTAL_BESVARET),0) AS ANSWERED_CALLS,							" +
			    "   NVL(SUM(BESVARET_25_SEK),0) AS ANSWERED_CALLS_25_SEC, 					" +
			    "   NVL(SUM(INTERN_KALD),0) AS INTERNAL_CALLS 								" +
			    "FROM 																		" +
			    "	KS_DRIFT.PERO_NKM_K�_OVERSIGT     										" +
			    "WHERE 																		" +
			    "	TIDSPUNKT >= '"+getStartDate(start,when)+"' AND        					" +
			    "   TIDSPUNKT <= '"+ slut+"' AND 											" +
			    "	TO_CHAR(TIDSPUNKT,'DY') NOT IN ('AB')     								" +
			    "GROUP BY																	" +
			    "	TRUNC(TIDSPUNKT,'"+getDateFormatString(when)+"'), 						" +
			    "	QUEUE   																" +
			    "ORDER BY																	" +
			    "	PERIOD 																	";
		ResultSet rs = getTable(sql);
		System.out.println(sql);
		try {
			while (rs.next()){
				for(CallQueue cq:al){
					String period = rs.getString("PERIOD");
					System.out.println(cq.getPeriod());
					System.out.println("Pause");
					System.out.println(period);
					String queue = rs.getString("QUEUE");
					double calls = rs.getDouble("CALLS");
					double answCalls = rs.getDouble("ANSWERED_CALLS");
					double answCalls25Sec = rs.getDouble("ANSWERED_CALLS_25_SEC");
					double internalCalls = rs.getDouble("INTERNAL_CALLS");
					if (cq.getPeriod().contentEquals(period)){
						if(queue.contentEquals("Hovednumre"))
							cq.setHovednumre(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));
						if(queue.contentEquals("S70151600"))
							cq.setS70151600(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));			
						if(queue.contentEquals("BoligejernesHus"))
							cq.setBoligejerneshus(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));
						if(queue.contentEquals("Privatcentre"))
							cq.setPrivatcentre(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));
						if(queue.contentEquals("Forsikring"))
							cq.setForsikring(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));			
						if(queue.contentEquals("Hotline"))
							cq.setHotline(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));
						if(queue.contentEquals("TekniskHotline"))
							cq.setTekniskhotline(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));
						if(queue.contentEquals("PensionHotline"))
							cq.setPensionhotline(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));
						if(queue.contentEquals("Stabe"))
							cq.setStabe(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));
						if(queue.contentEquals("Kunder"))
							cq.setKunder(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));
						if(queue.contentEquals("Erhverv"))
							cq.setErhverv(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));
						if(queue.contentEquals("ErhvervStorkunder"))
							cq.setErhvervstorkunder(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));
						if(queue.contentEquals("ErhvervOverflow"))
							cq.setErhvervoverflow(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));
						if(queue.contentEquals("BasisIntern"))
							cq.setBasisintern(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));
						if(queue.contentEquals("BoligIntern"))
							cq.setBoligintern(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));
						if(queue.contentEquals("InvesteringPension"))
							cq.setInvesteringpension(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));
						if(queue.contentEquals("Kredit"))
							cq.setKredit(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));
						if(queue.contentEquals("ForsikringIntern"))
							cq.setForsikringintern(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));
						if(queue.contentEquals("VIPservice"))
							cq.setVipservice(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));			
						if(queue.contentEquals("VIPcenterkald"))
							cq.setVipcenterkald(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));	
						if(queue.contentEquals("Datafejl"))
							cq.setDatafejl(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));				
						if(queue.contentEquals("Totalkredit"))
							cq.setTotalkredit(new CallType(calls, answCalls, answCalls25Sec,internalCalls,0));
						if(queue.contentEquals("NykreditKunder"))
							cq.setNykreditKunder(new CallType(calls, answCalls, answCalls25Sec,0,0));
					}
				}
			}
		} catch (SQLException e) {e.printStackTrace();}
		return al;
	}
	
	public Vector<CallQueue> getPostAndFax(String start, String slut,char when,Vector<CallQueue> al){
		String sql =
				"SELECT 																			" +
			    "	TO_CHAR(MIN(DA.DATO),'"+getDateDescriptionFormat(when)+"') AS PERIOD,			" +
			    "   SUM(NVL(POST,0)+NVL(FAX,0)) AS POST_FAX,										" +
			    "	SUM(FORSIKRING) AS FORSIKRING, 													" +
			    "	SUM(PERSONLIGE_KORT) AS PERSONLIGE_KORT 										" +
		        "FROM (                                                                             " +
		        "   SELECT * FROM KS_DRIFT.E3_STAT UNION                                            " +
		        "   SELECT * FROM KS_DRIFT.E3_SYSTEM UNION ALL SELECT * FROM KS_DRIFT.V_SOP_DATA    " +
		        ") E3 RIGHT OUTER JOIN KS_DRIFT.DATO DA ON DA.DATO=E3.DATO                          " +
			    "WHERE 																				" +
			    "	DA.DATO >= '"+getStartDate(start,when)+"' AND        							" +
			    "   DA.DATO <= '"+ slut+"' AND 														" +
			    "	TO_CHAR(DA.DATO,'DY') NOT IN ('AB')     										" +
			    "GROUP BY																			" +
			    "	TRUNC(DA.DATO,'"+getDateFormatString(when)+"') 									" +
			    "ORDER BY																			" +
			    "	PERIOD 																			";
		ResultSet rs = getTable(sql);
		try {
			while (rs.next()){
				for(CallQueue cq:al){
					if (cq.getPeriod().contentEquals(rs.getString("PERIOD"))){
						cq.setPostogfax(new CallType(rs.getDouble("POST_FAX"),0,0,0,0));
						cq.setForsikringsopgaver(new CallType(rs.getDouble("FORSIKRING"),0,0,0,0));
						cq.setPersonligekort(new CallType(rs.getDouble("PERSONLIGE_KORT"),0,0,0,0));
					}
				}
			}
		} catch (SQLException e) {e.printStackTrace();}
		return al;
	}
		
	public Vector<CallQueue> getEmail(String start, String slut,char when,Vector<CallQueue> al){
		String sql =
			    "   SELECT																			" +
			    "		TO_CHAR(MIN(AA.DATO),'"+getDateDescriptionFormat(when)+"') AS PERIOD, 		" +
				"   	SUM(KS_IND) AS EMAIL_INDG�ENDE,												" +
			    "       SUM(B9_IND) AS B9_IALT, 													" +
			    "       SUM(B9_FEJL) AS B9_OVERSKREDET, 											" +									
			    "       SUM(BEH_IND) AS BEH_IALT,													" +
			    "       SUM(BEH_FEJL) AS BEH_OVERSKREDET, 											" +
			    "       SUM(KAMP_IND) AS KAM_IALT, 													" +
			    "       SUM(KAMP_FEJL) AS KAM_OVERSKREDET 											" +
			    "   FROM 																			" +
			    "		KS_DRIFT.DATO AA                                                            " +
			    "   LEFT JOIN KS_DRIFT.EMAIL_STAT BB                                                " +
			    "   ON    AA.DATO = BB.DATO                                                         " +
			    "   WHERE 																			" +
			    "		AA.DATO >= '"+getStartDate(start,when)+"' AND                               " +
			    "   	AA.DATO <= '"+slut+"' AND 													" +
			    "		TO_CHAR(AA.DATO,'DY') NOT IN ('AB')                                         " +
			    "   GROUP BY TRUNC(AA.DATO,'"+getDateFormatString(when)+"')                         " +
			    "   ORDER BY 1 																		";
		ResultSet rs = getTable(sql);
		try {
			while (rs.next()){
				for(CallQueue cq:al){
					if (cq.getPeriod().contentEquals(rs.getString("PERIOD"))){
						cq.setEmailIndg�ende(new CallType(rs.getDouble("EMAIL_INDG�ENDE"),0,0,0,0));
						cq.setEmailB9(new CallType(rs.getDouble("B9_IALT"),0,0,0,rs.getDouble("B9_OVERSKREDET")));
						cq.setEmailBehandlede(new CallType(rs.getDouble("BEH_IALT"),0,0,0,rs.getDouble("BEH_OVERSKREDET")));
						cq.setEmailKampagne(new CallType(rs.getDouble("KAM_IALT"),0,0,0,rs.getDouble("KAM_OVERSKREDET")));
					}
				}
			}
		} catch (SQLException e) {
			
			e.printStackTrace();}
		return al;
	}

	  public Vector<CallQueue> getCallbacks(String startDate, String endDate, char c, Vector<CallQueue> data) {
	    DateTime end = new DateTime(endDate);
	    StringBuilder query = new StringBuilder();
	    query.append(" SELECT TO_CHAR(LAST_UPD,'" + getDateDescriptionFormat(c) + "') AS PERIOD, ");
	    query.append("      COUNT(CASE WHEN STATUS ='F�rdig' THEN 1 END) as completed_callbacks, ");
	    query.append("       COUNT(CASE WHEN SOLVED_SECONDS /60 /60 <= 2 THEN 1 END) as completed_within_2hours ");
	    query.append(" FROM   KS_DRIFT.NYK_SIEBEL_CALLBACK_AGENT_H_V ");
	    query.append(" WHERE  LAST_UPD  BETWEEN '" + getStartDate(startDate, c) + "' AND '" + end.getYear() + "-" + end.getMonthOfYear() + "-" + end.getDayOfMonth() + "' AND STATUS ='F�rdig' ");
	    query.append("GROUP BY TO_CHAR(LAST_UPD,'" + getDateDescriptionFormat(c) + "')");
	    ResultSet rs = getTable(query.toString());
	    try {
	      while (rs.next()) {
	        int completed = rs.getInt("completed_callbacks");
	        int completed_within_2hour = rs.getInt("completed_within_2hours");
	        String period = rs.getString("PERIOD");
	        for (CallQueue callQueue : data) {
	          if (callQueue.getPeriod().equalsIgnoreCase(period))
	            callQueue.setCallback(new CallType(completed, 0.0D, completed_within_2hour, 0.0D, 0.0D));
	        }
	      }
	    }
	    catch (SQLException e)
	    {
	      e.printStackTrace();
	    }
	    return data;
	  }
	  public Vector<CallQueue> getCallbacksCompletedDevision(String startDate, String endDate, char c, Vector<CallQueue> data)
	  {
	    DateTime end = new DateTime(endDate);

	    StringBuilder query = new StringBuilder();
	    query.append(" SELECT TO_CHAR(LAST_UPD,'" + getDateDescriptionFormat(c) + "') AS PERIOD, ");
	    query.append(" COUNT(CASE WHEN STATUS ='F�rdig' AND CB.INITIALS in TEAM.INITIALER THEN 1 end) as completed_outside_ks ");
	    query.append(" FROM     KS_DRIFT.NYK_SIEBEL_CALLBACK_AGENT_H_V CB INNER JOIN KS_DRIFT.V_TEAM_DATO team ON TO_CHAR(CB.CREATED,'YYYY-MM-DD') = TEAM.DATO");
	    query.append(" AND CB.LAST_UPD  BETWEEN '" + getStartDate(startDate, c) + "' AND '" + end.getYear() + "-" + end.getMonthOfYear() + "-" + end.getDayOfMonth() + "' ");
	    query.append(" GROUP BY TO_CHAR(LAST_UPD,'" + getDateDescriptionFormat(c) + "')");
	    ResultSet rs = getTable(query.toString());
	    try {
	      while (rs.next()) {
	        int completedOutsideKs = rs.getInt("completed_outside_ks");
	        String period = rs.getString("PERIOD");
	        for (CallQueue callQueue : data) {
	          if (callQueue.getPeriod().equalsIgnoreCase(period))
	            callQueue.getCallback().setExceeded(completedOutsideKs);
	        }
	      }
	    }
	    catch (SQLException e)
	    {
	      e.printStackTrace();
	    }
	    return data;
	  }
	  public Vector<CallQueue> getSupportOpgaver(String start, String slut, char when, Vector<CallQueue> al) {
		    String sql = "Select TO_CHAR(MIN(COMPLETED_DATE), '" + getDateDescriptionFormat(when) + "') AS PERIOD, COUNT(*) AS TASKCOUNT " + 
		      "from KS_DRIFT.SOP_TASK_DATA " + 
		      "WHERE COMPLETED_DATE BETWEEN '" + getStartDate(start, when) + "' AND '" + slut + "' AND TO_CHAR(COMPLETED_DATE, 'DY') NOT IN ('AB') AND " + 
		      "TASK_ID NOT IN(65,67 , 83, 84, 85, 57, 56, 54) AND DELETED NOT IN (1) GROUP BY TRUNC(COMPLETED_DATE, '" + getDateFormatString(when) + "')";
		    ResultSet rs = getTable(sql);
		    try {
		      while (rs.next()) {
		        for (CallQueue callQueue : al) {
		          if (callQueue.getPeriod().contentEquals(rs.getString("PERIOD")))
		            callQueue.setSupportOpgaver(new CallType(rs.getDouble("TASKCOUNT"), 0.0D, 0.0D, 0.0D, 0.0D));
		        }
		      }
		    }
		    catch (SQLException e)
		    {
		      e.printStackTrace();
		    }
		    return al;
		  }
	  
	  public Vector<CallQueue> getOTBOpgaver(String start, String slut, char when, Vector<CallQueue> al) {
		    String sql = "Select TO_CHAR(MIN(COMPLETED_DATE), '" + getDateDescriptionFormat(when) + "') AS OTB_PERIOD, COUNT(*) AS OTB_TASKCOUNT " + 
		      "from KS_DRIFT.OOP_TASK_DATA " + 
		      "WHERE COMPLETED_DATE BETWEEN '" + getStartDate(start, when) + "' AND '" + slut + "' AND TO_CHAR(COMPLETED_DATE, 'DY') NOT IN ('AB') AND " + 
		      "DELETED NOT IN (1) GROUP BY TRUNC(COMPLETED_DATE, '" + getDateFormatString(when) + "')";
		    ResultSet rs = getTable(sql);
		    try {
		      while (rs.next()) {
		        for (CallQueue callQueue : al) {
		          if (callQueue.getPeriod().contentEquals(rs.getString("OTB_PERIOD")))
		            callQueue.setOTBOpgaver(new CallType(rs.getDouble("OTB_TASKCOUNT"), 0.0D, 0.0D, 0.0D, 0.0D));
		        }
		      }
		    }
		    catch (SQLException e)
		    {
		      e.printStackTrace();
		    }
		    return al;
		  }
	  
	  public Vector<CallQueue> getWebdesk(String start, String enddate, char c, Vector<CallQueue> al) {
		    DateTime end = new DateTime(enddate);
		    DateTime newEndDate = end.plusDays(1);

		    String sql2 = "Select TO_CHAR(SUM_DATE,'" + 
		      getDateDescriptionFormat(c) + "') AS PERIOD, " + 
		      "SUM(ANSWERED_SC) AS ANSWERED, " + 
		      "SUM(CALLS_SC) AS TOTAL_COUNT " + 
		      "FROM KS_DRIFT.V_WEBDESK_SERVICECENTER_SUM WHERE DEPARTMENT_ID = '42' AND SUM_DATE BETWEEN '" + getStartDate(start, c) + "' AND '" + newEndDate.getYear() + "-" + newEndDate.getMonthOfYear() + "-" + newEndDate.getDayOfMonth() + "' GROUP BY TO_CHAR(SUM_DATE,'" + getDateDescriptionFormat(c) + "') ORDER BY PERIOD";
		    
		    System.out.println(sql2);
		    ResultSet rs2 = getTable(sql2);
		    try {
		      while (rs2.next()) {
		        Integer answered = Integer.valueOf(rs2.getInt("ANSWERED"));
		        Integer total_count = Integer.valueOf(rs2.getInt("TOTAL_COUNT"));
		        String period = rs2.getString("PERIOD");
		        for (CallQueue callQueue : al) {
		          if (callQueue.getPeriod().equalsIgnoreCase(period))
		            callQueue.setWebdeskPercentageOfTotalAnswered(new CallType(total_count.intValue(), 0.0D, answered.intValue(), 0.0D, 0.0D));
		        }
		      }
		    }
		    catch (SQLException e)
		    {
		      e.printStackTrace();
		    }
		    return al;
		  }
	  public Vector<CallQueue> getWebdesk2013(String startDate, String endDate, char c, Vector<CallQueue> al) {
		    DateTime end = new DateTime(endDate);
		    DateTime newEndDate = end.plusDays(1);
		    String sql2 = "Select TO_CHAR(SUM_DATE,'" + 
		      getDateDescriptionFormat(c) + "') AS PERIOD, " + 
		      "SUM(ANWERED_WITHIN_25_SEC_SC) AS ANSWERED_WITHIN_25_SEC, " + 
		      "SUM(CALLS_SC) AS TOTAL_COUNT " + 
		      "FROM KS_DRIFT.V_WEBDESK_SERVICECENTER_SUM WHERE SUM_DATE BETWEEN '" + getStartDate(startDate, c) + "' AND '" + newEndDate.getYear() + "-" + newEndDate.getMonthOfYear() + "-" + newEndDate.getDayOfMonth() + 
		      "' GROUP BY TO_CHAR(SUM_DATE,'" + getDateDescriptionFormat(c) + "') ORDER BY PERIOD";
		    
		    System.out.println(sql2);
		    ResultSet rs2 = getTable(sql2);
		    try {
		      while (rs2.next())
		      {
		        Integer answered_within_30 = Integer.valueOf(rs2.getInt("ANSWERED_WITHIN_25_SEC"));
		        Integer total_count = Integer.valueOf(rs2.getInt("TOTAL_COUNT"));
		        String period = rs2.getString("PERIOD");

		        for (CallQueue callQueue : al) {
		          if (callQueue.getPeriod().equalsIgnoreCase(period))
		            callQueue.setWebdesk2013(new CallType(total_count.intValue(), answered_within_30.intValue(), answered_within_30.intValue(), 0.0D, 0.0D));
		        }
		      }
		    }
		    catch (SQLException e)
		    {
		      e.printStackTrace();
		    }
		    return al;
		  }
}
