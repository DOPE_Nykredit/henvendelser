
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class SplashScreen extends JDialog {
	private static final long serialVersionUID = 1L;
	private Color background = new Color(34,56,127);
	private Color foreground = Color.white;
	
	public SplashScreen(String programName,String programDescription){
		initialize(programName,programDescription);
	}
	public SplashScreen(String programName){
		initialize(programName,"");
	}	
	
	private void initialize(String programName,String programDescription){
		try {
	        UIManager.setLookAndFeel(
	            UIManager.getSystemLookAndFeelClassName());
	    } catch (Exception e) {}
		this.setSize(400, 150);
		this.setLocationRelativeTo(this.getRootPane());
		this.setUndecorated(true);
		this.setContentPane(getMainPane(programName,programDescription));
		this.setTitle(programName);
		this.setVisible(true);
	}
	
	private JPanel getMainPane(String programName,String NameDescription){
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.weighty = 0.8d;
		p.add(getHeadline(programName),c);
		c.gridx = 0;
		c.gridy = 1;
		c.weighty = 0.2d;
		p.add(getSubHeadline(NameDescription),c);
		p.setOpaque(true);
		p.setBackground(background);
		return p;
	}
	private JLabel getHeadline(String programName){
		JLabel l = new JLabel(programName,JLabel.CENTER);
		l.setOpaque(true);
		l.setBackground(background);
		l.setForeground(foreground);
		l.setFont(new Font("Verdana", Font.ITALIC, 25));
		return l;
	}
	private JLabel getSubHeadline(String programName){
		JLabel l = new JLabel(programName,JLabel.CENTER);
		l.setOpaque(true);
		l.setBackground(background);
		l.setForeground(foreground);
		l.setFont(new Font("Verdana", Font.ITALIC, 10));
		return l;
	}
	
}
