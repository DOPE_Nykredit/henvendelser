
import java.util.ArrayList;

public class TaskStatistics {
	
	private ArrayList<TaskData> allTasks = new ArrayList<TaskData>();
	
	private TaskData getTaskData(TaskData.TaskTypes type){
		for(TaskData s : this.allTasks)
			if(s.getTaskType() == type)
				return s;
		
		return null;
	}

//	public double getHovednumreBesvarelsesPct25(){
//		return 
//				(hovednumre.getAnsweredCalls25Seconds()+s70151600.getAnsweredCalls25Seconds())/
//				(hovednumre.getCalls()+s70151600.getCalls());
//	}
//	
//	public double getPrivatcentreBesvarelsesPct25(){
//		return 
//				(boligejerneshus.getAnsweredCalls25Seconds()+privatcentre.getAnsweredCalls25Seconds())/
//				(boligejerneshus.getCalls()+privatcentre.getCalls());
//	}	
//	
//	public double getForsikringBesvarelsesPct25(){
//		return
//				forsikring.getAnsweredCalls25Seconds()/
//				forsikring.getCalls();
//	}		
//	
//	public double getHotlineBesvarelsesPct25(){
//		return 
//				(hotline.getAnsweredCalls25Seconds()+tekniskhotline.getAnsweredCalls25Seconds())/
//				(hotline.getCalls()+tekniskhotline.getCalls());
//	}
//	
//	public double getStabeBesvarelsesPct25(){
//		return (stabe.getAnsweredCalls25Seconds() / stabe.getCalls());
//	}
//	
//	public double getKunderBesvarelsesPct25(){
//		return (kunder.getAnsweredCalls25Seconds() / kunder.getCalls());
//	}	
//	
//	public double getErhvervBesvarelsesPct25(){
//		return (erhverv.getAnsweredCalls25Seconds()+erhvervstorkunder.getAnsweredCalls25Seconds()+erhvervoverflow.getAnsweredCalls25Seconds()) / (erhverv.getCalls()+erhvervstorkunder.getCalls()+erhvervoverflow.getCalls());
//	}
//	
//	public double getVipServiceBesvarelsesPct25(){
//		return (vipservice.getAnsweredCalls25Seconds() + vipcenterkald.getAnsweredCalls25Seconds()) / (vipservice.getCalls()+vipcenterkald.getCalls());
//	}
//	
//	public double getKundeserviceBesvarelsesPct25(){
//		return (getKundeserviceCallOnlyTotal().getAnsweredCalls25Seconds() / getKundeserviceCallOnlyTotal().getCalls());
//	}
//	
//	public double getKundeserviceBesvarelsesPct(){
//		return (getKundeserviceCallOnlyTotal().getAnsweredCalls() / getKundeserviceCallOnlyTotal().getCalls());
//	}
//	
//	public double getStabeBesvarelsesPct(){
//		return (stabe.getAnsweredCalls() / stabe.getCalls());
//	}
//	
//	public double getEmailStatBesvarelsesPct(){
//		return ((getEmailStatIalt() - getEmailStatIaltOverskredet()) / getEmailStatIalt());	
//	}	
//	
//	public double getEmailStatIalt(){
//		return (emailB9.getCalls() + emailBehandlede.getCalls());
//	}
//
//	public double getEmailStatIaltOverskredet(){
//		return (emailB9.getExceeded() + emailBehandlede.getExceeded());
//	}
//
//	public double getWebdeskBesvarelsesPct(){
//		return (webdesk.getAnsweredCalls() / webdesk.getCalls());
//	}
//	
//	public double getB9BesvarelsesPct(){
//		return ((emailB9.getCalls()-emailB9.getExceeded()) / emailB9.getCalls());
//	}
	
	public TaskData getKundeserviceCallOnlyTotal(){
		int totalCalls = 0;
		int answeredCalls = 0;
		int answeredCalls25Seconds = 0;		
		int internalCalls = 0;
		int exceededCalls = 0;
		
		for(TaskData d : this.allTasks)
			if(d.getTaskType().IsCall()){
				totalCalls += d.getTotalReceived();
				answeredCalls += d.getHandled();
				answeredCalls25Seconds += d.getHandledWithinLimits();
				internalCalls += d.getInternalCalls();
				exceededCalls += d.getExceeded();
			}

		return new TaskData(null, 
							totalCalls,
				            answeredCalls,
				            answeredCalls25Seconds,
				            internalCalls,
				            exceededCalls); 
	}
	
	public TaskData getKundeserviceTotal(){
		int totalCalls = 0;
		int answeredCalls = 0;
		int answeredCalls25Seconds = 0;		
		int internalCalls = 0;
		int exceededCalls = 0;
		
		for(TaskData d : this.allTasks){
			totalCalls += d.getTotalReceived();
			answeredCalls += d.getHandled();
			answeredCalls25Seconds += d.getHandledWithinLimits();
			internalCalls += d.getInternalCalls();
			exceededCalls += d.getExceeded();
		}

		return new TaskData(null, 
							totalCalls,
				            answeredCalls,
				            answeredCalls25Seconds,
				            internalCalls,
				            exceededCalls); 
	}
}