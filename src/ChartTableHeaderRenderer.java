
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;


public class ChartTableHeaderRenderer extends JPanel implements TableCellRenderer {

	private static final long serialVersionUID = 1L;
	private boolean legend;
	private Component component;
	
	 public ChartTableHeaderRenderer(boolean legend,Component component){
		 this.legend = legend;
		 this.component = component;
	 }
	
    public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected, boolean hasFocus, int rowIndex, int vColIndex) {
    	
    	this.setLayout(new GridBagLayout());
    	GridBagConstraints c = new GridBagConstraints();
   	    	
    	c.fill = GridBagConstraints.BOTH;
    	c.weightx = 1;
    	c.weighty = 1;
    	
    	JLabel text = new JLabel("<html><p align=center>"+(String)value,JLabel.CENTER);
    	
    	this.addMouseListener(new ChartWithTableMouseListener(component));
    	text.addMouseListener(new ChartWithTableMouseListener(component));
    	
    	this.removeAll();
    	this.add(text,c);
    	
    	this.setOpaque(true);
    	this.setBackground(Color.white);
    	
    	text.setOpaque(true);
    	text.setBackground(Color.white);
    	text.setFont(new Font("Arial",Font.PLAIN,9));
    	if(legend){
    		if (vColIndex == 1){setBorder(BorderFactory.createMatteBorder(1, 0, 1, 0, Color.gray));
    		}else{setBorder(BorderFactory.createMatteBorder(1, 1, 1, 0, Color.gray));}
    	}else {setBorder(BorderFactory.createMatteBorder(1, 1, 1, 0, Color.gray));}
    	
        return this;
    }
    
}

