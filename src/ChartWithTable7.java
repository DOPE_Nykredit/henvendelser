import com.nykredit.kundeservice.swing.NTable;
import com.nykredit.kundeservice.util.Formatter;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.util.ListIterator;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.CombinedDomainCategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.StackedBarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RectangleInsets;
public class ChartWithTable7 extends JPanel
{
  private static final long serialVersionUID = 1L;
  private JFreeChart chart;
  private ChartPanel chartPanel;
  public static JLabel l;
  private JTable table = new NTable() {
    private static final long serialVersionUID = 1L;

    public boolean isCellEditable(int rowIndex, int colIndex) { return false; }
  };

  private DefaultTableModel tableModel = new DefaultTableModel();
  private DefaultCategoryDataset datasetWithoutTotal;
  private DefaultCategoryDataset datasetWithTotal;
  private Formatter bf = new Formatter();
  private Dimension dimensionChart;
  private Dimension dimensionTable;

  public ChartWithTable7(boolean stackedBarChart, int tableheight, String helpText) {
    this.dimensionChart = new Dimension(540, 140);
    this.dimensionTable = new Dimension(555, 155);
    setLayout(new GridBagLayout());
    setOpaque(true);
    setBackground(Color.white);
    GridBagConstraints c = new GridBagConstraints();
    c.gridy = 0;
    c.fill = 1;
    c.gridx = 0;
    add(getSpacingLabel(this.dimensionChart.height), c);
    c.weightx = 1.0D;
    c.weighty = 1.0D;
    c.gridy = 0;
    c.gridx = 1;
    add(getChartPanel(stackedBarChart), c);
    c.gridwidth = 2;
    c.gridx = 0;
    c.gridy = 1;
    add(getTableWithScrollPane(), c);
  }

  private JLabel getSpacingLabel(int height)
  {
    l = new JLabel();
    l.setMinimumSize(new Dimension(92, height));
    l.setPreferredSize(new Dimension(92, height));
    l.setOpaque(true);
    l.setBackground(Color.white);
    l.addMouseListener(new ChartWithTableMouseListener(this));
    return l;
  }
  public ChartWithTable7() {
    this.dimensionTable = new Dimension(655, 700);
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.gridy = 0;
    c.fill = 1;
    c.weightx = 1.0D;
    c.weighty = 1.0D;
    add(getTableWithScrollPane(), c);
  }
  private ChartPanel getChartPanel(boolean stackedBarChart) {
    this.chartPanel = new ChartPanel(this.chart, false, false, false, false, false);
    this.chartPanel.setPreferredSize(this.dimensionChart);
    this.chartPanel.setMinimumSize(this.dimensionChart);
    if (stackedBarChart) this.chart = createStackedBarChart("");
    if (!stackedBarChart) this.chart = createLineChart("");
    setBorder(BorderFactory.createLineBorder(Color.black));
    this.chartPanel.addMouseListener(new ChartWithTableMouseListener(this));
    Label l = new Label("Heeeeeeelp");
    return this.chartPanel;
  }
  public void setHenvendelser(char d, Vector<CallQueue> data) {
    this.datasetWithoutTotal = createDataSetHenvendelser(data, false);
    this.datasetWithTotal = createDataSetHenvendelser(data, true);
    String periode;
    switch (d) { case 'D':
      periode = "Henvendelser i KS"; break;
    case 'W':
      periode = "Henvendelser i KS"; break;
    case 'M':
      periode = "Henvendelser i KS"; break;
    case 'Y':
      periode = "Henvendelser i KS"; break;
    default:
      return;
    }
    String title = periode;
    this.chart = createStackedBarChart(title);
    this.chartPanel.setPreferredSize(this.dimensionChart);
    this.chartPanel.setChart(this.chart);
    setTableModel(true, false);
  }

  public void setEmailData(char d, Vector<CallQueue> data) {
    this.datasetWithoutTotal = createDataSetEmailData(data, false);
    this.datasetWithTotal = createDataSetEmailData(data, true);
    String periode;
    switch (d) { case 'D':
      periode = "Besvarelsesprocenter i KS"; break;
    case 'W':
      periode = "Besvarelsesprocenter i KS"; break;
    case 'M':
      periode = "Besvarelsesprocenter i KS"; break;
    case 'Y':
      periode = "Besvarelsesprocenter i KS"; break;
    default:
      return;
    }
    String title = periode;
    this.chart = createLineChart(title);
    this.chartPanel.setPreferredSize(this.dimensionChart);
    this.chartPanel.setChart(this.chart);

    setTableModel(true, true);
  }

  public void setEmailHenvendelser(char d, Vector<CallQueue> data) {
    this.datasetWithoutTotal = createDataSetEmailHenvendelser(data, false);
    this.datasetWithTotal = createDataSetEmailHenvendelser(data, true);
    String periode;
    switch (d) { case 'D':
      periode = "E-mail antal"; break;
    case 'W':
      periode = "E-mail antal"; break;
    case 'M':
      periode = "E-mail antal"; break;
    case 'Y':
      periode = "E-mail antal"; break;
    default:
      return;
    }
    String title = periode;
    this.chart = createStackedBarChart(title);
    this.chartPanel.setChart(this.chart);
    setTableModel(true, false);
  }

  public void setBesvarelsesProcenter25Sec(char d, Vector<CallQueue> data) {
    this.datasetWithoutTotal = createDataSetBesvarelsesProcenter25Sec(data, false);
    this.datasetWithTotal = createDataSetBesvarelsesProcenter25Sec(data, true);
    String periode;
    switch (d) { case 'D':
      periode = "Besvarelsesprocenter i KS"; break;
    case 'W':
      periode = "Besvarelsesprocenter i KS"; break;
    case 'M':
      periode = "Besvarelsesprocenter i KS"; break;
    case 'Y':
      periode = "Besvarelsesprocenter i KS"; break;
    default:
      return;
    }
    String title = periode;
    this.chart = createLineChart(title);
    this.chartPanel.setChart(this.chart);
    setTableModel(true, true);
  }

  public void setBesvarelsesProcenter(char D, Vector<CallQueue> data) {
    this.datasetWithoutTotal = createDataSetBesvarelsesProcenter(data, false);
    this.datasetWithTotal = createDataSetBesvarelsesProcenter(data, true);
    String periode;
    switch (D) { case 'D':
      periode = "Besvarede henvendelser"; break;
    case 'W':
      periode = "Besvarede henvendelser"; break;
    case 'M':
      periode = "Besvarede henvendelser"; break;
    case 'Y':
      periode = "Besvarede henvendelser"; break;
    default:
      return;
    }
    String title = periode;
    this.chart = createLineChart(title);
    this.chartPanel.setChart(this.chart);
    setTableModel(true, true);
  }

  public void setEmailBesvarelsesProcenter(char D, Vector<CallQueue> data) {
    this.datasetWithoutTotal = createDataSetEmailBesvarelsesProcenter(data, false);
    this.datasetWithTotal = createDataSetEmailBesvarelsesProcenter(data, true);
    String periode;
    switch (D) { case 'D':
      periode = "E-mail håndtering "; break;
    case 'W':
      periode = "E-mail håndtering "; break;
    case 'M':
      periode = "E-mail håndtering "; break;
    case 'Y':
      periode = "E-mail håndtering "; break;
    default:
      return;
    }
    String title = periode;
    this.chart = createLineChart(title);
    this.chartPanel.setChart(this.chart);
    setTableModel(true, true);
  }

  public void setInterneHenvendelser(char D, Vector<CallQueue> data) {
    this.datasetWithoutTotal = createDataSetInterneHenvendelser(data, false);
    this.datasetWithTotal = createDataSetInterneHenvendelser(data, true);
    String periode;
    switch (D) { case 'D':
      periode = "Interne Henvendelser i KS "; break;
    case 'W':
      periode = "Interne Henvendelser i KS "; break;
    case 'M':
      periode = "Interne Henvendelser i KS "; break;
    case 'Y':
      periode = "Interne Henvendelser i KS "; break;
    default:
      return;
    }
    String title = periode;
    this.chart = createStackedBarChart(title);
    this.chartPanel.setChart(this.chart);
    setTableModel(true, false);
  }

  private DefaultCategoryDataset createDataSetHenvendelser(Vector<CallQueue> data, boolean total)
  {
    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    for (CallQueue col : data) {
      dataset.addValue(col.getHovednumre().getCalls() + col.getS70151600().getCalls(), "Hovednumre", col.getPeriod());
      dataset.addValue(col.getBoligejerneshus().getCalls() + col.getPrivatcentre().getCalls(), "Privatcentre", col.getPeriod());
      
      if (col.getForsikring().getCalls() > 0.0D) {
    	  dataset.addValue(col.getForsikring().getCalls(), "Forsikring", col.getPeriod());
        }      

      dataset.addValue(col.getHotline().getCalls() + col.getTekniskhotline().getCalls(), "Hotline+TekniskHotline", col.getPeriod());

      if (col.getPensionhotline().getCalls() > 0.0D) {
        dataset.addValue(col.getPensionhotline().getCalls(), "PensionHotline", col.getPeriod());
      }

      dataset.addValue(col.getStabe().getCalls(), "Stabe", col.getPeriod());
      dataset.addValue(col.getKunder().getCalls(), "Kunder", col.getPeriod());
      dataset.addValue(col.getErhverv().getCalls() + col.getErhvervstorkunder().getCalls() + col.getErhvervoverflow().getCalls(), "Erhverv", col.getPeriod());
      if (col.getNykreditKunder().getCalls() != 0.0D) {
        dataset.addValue(col.getNykreditKunder().getCalls(), "NykreditKunder", col.getPeriod());
      }
      if (col.getVipservice().getCalls() + col.getVipcenterkald().getCalls() != 0.0D) {
          dataset.addValue(col.getVipservice().getCalls() + col.getVipcenterkald().getCalls(), "VIPservice", col.getPeriod());
        }
      if (col.getTotalkredit().getCalls() != 0.0D) {
          dataset.addValue(col.getTotalkredit().getCalls(), "Totalkredit", col.getPeriod());
        }

      dataset.addValue(col.getWebdesk2013().getCalls(), "Webdesk", col.getPeriod());
      dataset.addValue(col.getEmailIndgående().getCalls(), "E-mails", col.getPeriod());
      
      dataset.addValue(col.getSupportOpgaver().getCalls(), "Supportopgaver", col.getPeriod());
      
      if (col.getOTBOpgaver().getCalls() != 0.0D) {
    	  dataset.addValue(col.getOTBOpgaver().getCalls(), "Overtræksopgaver", col.getPeriod());
      } 
      
      if (col.getBasisintern().getCalls() + col.getBoligintern().getCalls() + col.getInvesteringpension().getCalls() + col.getKredit().getCalls() + col.getForsikringintern().getCalls()!= 0.0D) {
        dataset.addValue(col.getBasisintern().getCalls() + col.getBoligintern().getCalls() + col.getInvesteringpension().getCalls() + col.getKredit().getCalls() + col.getForsikringintern().getCalls(), "Ekst kald 2nd level", col.getPeriod());
      }          
     
    }

    for (CallQueue col : data) {
      if (total) dataset.addValue(col.getKundeserviceTotal().getCalls() + col.getWebdesk2013().getCalls(), "Total", col.getPeriod());
    }

    return dataset;
  }

  private DefaultCategoryDataset createDataSetEmailData(Vector<CallQueue> data, boolean total) {
    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    for (CallQueue col : data) {
      dataset.addValue(col.getKundeserviceBesvarelsesPct25() * 100.0D, "Telefonisk(80/25)", col.getPeriod());

      dataset.addValue(col.getEmailStatBesvarelsesPct() * 100.0D, "E-mail håndtering(4 timer)", col.getPeriod());
      dataset.addValue(col.getWebdesk2013percentage() * 100.0D, "Webdesk(80/25)", col.getPeriod());
    }

    return dataset;
  }
  private DefaultCategoryDataset createDataSetNewEmailData(Vector<CallQueue> data, boolean total) {
    DefaultCategoryDataset dataset = new DefaultCategoryDataset();

    for (CallQueue col : data) {
      dataset.addValue(col.getEmailIndgående().getCalls(), "E-mail Indgående", col.getPeriod());
      dataset.addValue(col.getEmailBehandlede().getCalls(), "E-mail Behandlet i alt", col.getPeriod());
      dataset.addValue(col.getEmailBehandlede().getExceeded(), "E-mail overskredet", col.getPeriod());
      dataset.addValue(col.getEmailB9().getCalls(), "B9 total", col.getPeriod());
      dataset.addValue(col.getEmailB9().getExceeded(), "B9 overskredet", col.getPeriod());
      dataset.addValue(col.getEmailKampagne().getCalls(), "Kampagne i alt", col.getPeriod());
      if (col.getEmailKampagne().getExceeded() != 0.0D) {
        dataset.addValue(col.getEmailKampagne().getExceeded(), "Kampagne overskredet", col.getPeriod());
      }
    }
    return dataset;
  }

  private DefaultCategoryDataset createDataSetEmailHenvendelser(Vector<CallQueue> data, boolean total) {
    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    for (CallQueue col : data) {
      dataset.addValue(col.getEmailStatIalt(), "E-mail i alt u. kampagne", col.getPeriod());
      dataset.addValue(col.getEmailKampagne().getCalls(), "Kampagnemails", col.getPeriod());
    }
    return dataset;
  }

  private DefaultCategoryDataset createDataSetBesvarelsesProcenter25Sec(Vector<CallQueue> data, boolean total) {
    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    for (CallQueue col : data) {
      dataset.addValue(col.getHovednumreBesvarelsesPct25() * 100.0D, "Hovednumre", col.getPeriod());
      dataset.addValue(col.getPrivatcentreBesvarelsesPct25() * 100.0D, "Privatcentre", col.getPeriod());
      if (col.getForsikringBesvarelsesPct25() * 100.0D > 0.0D) {
    	  dataset.addValue(col.getForsikringBesvarelsesPct25() * 100.0D, "Forsikring", col.getPeriod());
        }      
      dataset.addValue(col.getHotlineBesvarelsesPct25() * 100.0D, "Hotline", col.getPeriod());
      dataset.addValue(col.getStabeBesvarelsesPct25() * 100.0D, "Stabe", col.getPeriod());
      dataset.addValue(col.getKunderBesvarelsesPct25() * 100.0D, "Kunder", col.getPeriod());

      if (col.getVipServiceBesvarelsesPct25() != 0.0D)
      {
        dataset.addValue(col.getVipServiceBesvarelsesPct25() * 100.0D, "VIPservice", col.getPeriod());
      }

      if (col.getErhvervBesvarelsesPct25() != 0.0D) {
        dataset.addValue(col.getErhvervBesvarelsesPct25() * 100.0D, "Erhverv", col.getPeriod());
      }

      if (col.getErhvervBesvarelsesPct25() != 0.0D) {
        dataset.addValue(col.getErhvervBesvarelsesPct25() * 100.0D, "NykreditKunder", col.getPeriod());
      }
      
      if (col.getTotalKreditBesvarelsesPct25() != 0.0D) {
          dataset.addValue(col.getTotalKreditBesvarelsesPct25(), "Totalkredit", col.getPeriod());
        }
      
    }
    
    for (CallQueue col : data) {
      dataset.addValue(col.getKundeserviceBesvarelsesPct25() * 100.0D, "Kundeservice og Salg", col.getPeriod());
    }
    return dataset;
  }

  private DefaultCategoryDataset createDataSetBesvarelsesProcenter(Vector<CallQueue> data, boolean total) {
    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    for (CallQueue col : data)
    {
      dataset.addValue(col.getKundeserviceBesvarelsesPct() * 100.0D, "Telefoner i KS", col.getPeriod());
      dataset.addValue(col.getEmailStatBesvarelsesPct() * 100.0D, "E-mails", col.getPeriod());
      dataset.addValue(col.getWebdeskPercentageOfTotalAnswered(), "Webdesk", col.getPeriod());
    }
    return dataset;
  }

  private DefaultCategoryDataset createDataSetEmailBesvarelsesProcenter(Vector<CallQueue> data, boolean total) {
    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    for (CallQueue col : data) {
      dataset.addValue(col.getB9BesvarelsesPct() * 100.0D, "E-mail håndtering MS", col.getPeriod());
      dataset.addValue(col.getEmailStatBesvarelsesPct() * 100.0D, "E-mail håndtering I alt", col.getPeriod());
    }
    return dataset;
  }

  private DefaultCategoryDataset createDataSetInterneHenvendelser(Vector<CallQueue> data, boolean total)
  {
    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    for (CallQueue col : data) {
      dataset.addValue(col.getHovednumre().getInternalCalls() + col.getS70151600().getInternalCalls(), "Hovednumre", col.getPeriod());
      dataset.addValue(col.getBoligejerneshus().getInternalCalls() + col.getPrivatcentre().getInternalCalls(), "Privatcentre", col.getPeriod());
      if (col.getForsikring().getInternalCalls() > 0.0D) {
    	  dataset.addValue(col.getForsikring().getInternalCalls(), "Forsikring", col.getPeriod());
        }     
      dataset.addValue(col.getHotline().getInternalCalls() + col.getTekniskhotline().getInternalCalls(), "Hotline+TekniskHotline", col.getPeriod());

      if (col.getPensionhotline().getInternalCalls() != 0.0D) {
        dataset.addValue(col.getPensionhotline().getInternalCalls(), "PensionHotline", col.getPeriod());
      }

      dataset.addValue(col.getStabe().getInternalCalls(), "Stabe", col.getPeriod());
      dataset.addValue(col.getKunder().getInternalCalls(), "Kunder", col.getPeriod());

      if (col.getErhverv().getInternalCalls() + col.getErhvervstorkunder().getInternalCalls() + col.getErhvervoverflow().getInternalCalls() != 0.0D) {
        dataset.addValue(col.getErhverv().getInternalCalls() + col.getErhvervstorkunder().getInternalCalls() + col.getErhvervoverflow().getInternalCalls(), "Erhverv", col.getPeriod());
      }
      if (col.getVipservice().getInternalCalls() + col.getVipcenterkald().getInternalCalls() != 0.0D) {
        dataset.addValue(col.getVipservice().getInternalCalls() + col.getVipcenterkald().getInternalCalls(), "VIPservice", col.getPeriod());
      }

      dataset.addValue(col.getBasisintern().getInternalCalls(), "BasisIntern", col.getPeriod());

      if (col.getBoligintern().getInternalCalls() != 0.0D) {
        dataset.addValue(col.getBoligintern().getInternalCalls(), "BoligIntern", col.getPeriod());
      }
      if (col.getInvesteringpension().getInternalCalls() != 0.0D) {
        dataset.addValue(col.getInvesteringpension().getInternalCalls(), "Investering & Pension", col.getPeriod());
      }

      if (col.getKredit().getInternalCalls() != 0.0D) {
        dataset.addValue(col.getKredit().getInternalCalls(), "Kredit", col.getPeriod());
      }

      if (col.getForsikringintern().getInternalCalls() != 0.0D) {
        dataset.addValue(col.getForsikringintern().getInternalCalls(), "ForsikringIntern", col.getPeriod());
      }

      if (col.getTotalkredit().getInternalCalls() != 0.0D) {
        dataset.addValue(col.getTotalkredit().getInternalCalls(), "Totalkredit", col.getPeriod());
      }

    }

    for (CallQueue callQueue : data) {
      if (total) dataset.addValue(callQueue.getKundeserviceTotal().getInternalCalls(), "Total", callQueue.getPeriod());

    }

    return dataset;
  }

  private JFreeChart createStackedBarChart(String title)
  {
    JFreeChart chart = ChartFactory.createStackedBarChart(title, null, null, this.datasetWithoutTotal, PlotOrientation.VERTICAL, false, false, false);

    chart.setBackgroundPaint(Color.white);

    CategoryPlot plot = (CategoryPlot)chart.getPlot();
    plot.setBackgroundPaint(Color.white);
    plot.setRangeGridlinePaint(Color.black);

    StackedBarRenderer r = (StackedBarRenderer)chart.getCategoryPlot().getRenderer();
    r.setDrawBarOutline(false);
    r.setSeriesPaint(0, new Color(64, 105, 156));
    r.setSeriesPaint(1, new Color(158, 65, 62));
    r.setSeriesPaint(2, new Color(127, 154, 72));
    r.setSeriesPaint(3, new Color(105, 81, 133));
    r.setSeriesPaint(4, new Color(60, 141, 163));
    r.setSeriesPaint(5, new Color(204, 123, 56));
    r.setSeriesPaint(6, new Color(79, 129, 189));
    r.setSeriesPaint(7, new Color(198, 214, 172));
    r.setSeriesPaint(8, new Color(217, 170, 169));
    r.setSeriesPaint(9, new Color(247, 150, 70));
    r.setSeriesPaint(10, new Color(186, 176, 201));
    r.setSeriesPaint(11, new Color(170, 186, 215));
    r.setSeriesPaint(12, new Color(155, 187, 89));
    r.setSeriesPaint(13, new Color(169, 206, 220));
    r.setSeriesPaint(14, new Color(192, 80, 77));
    r.setSeriesPaint(15, new Color(128, 100, 162));
    r.setSeriesPaint(16, new Color(190, 115, 32));

    r.setShadowVisible(false);
    r.setBase(0.0D);

    CategoryAxis domainAxis = plot.getDomainAxis();
    domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
    domainAxis.setLowerMargin(0.0D);
    domainAxis.setUpperMargin(0.0D);
    domainAxis.setVisible(false);

    NumberAxis rangeAxis = (NumberAxis)plot.getRangeAxis();
    rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    rangeAxis.setAutoRangeIncludesZero(false);

    return chart;
  }
  private JFreeChart createDualAxisChart(String title) {
    JFreeChart chart = ChartFactory.createStackedBarChart(title, null, null, this.datasetWithoutTotal, PlotOrientation.VERTICAL, false, false, false);

    chart.setBackgroundPaint(Color.white);

    CategoryPlot plot = (CategoryPlot)chart.getPlot();
    plot.setBackgroundPaint(Color.white);
    plot.setRangeGridlinePaint(Color.black);

    StackedBarRenderer r = (StackedBarRenderer)chart.getCategoryPlot().getRenderer();
    r.setDrawBarOutline(false);
    r.setSeriesPaint(0, new Color(64, 105, 156));
    r.setSeriesPaint(1, new Color(158, 65, 62));
    r.setSeriesPaint(2, new Color(127, 154, 72));
    r.setSeriesPaint(3, new Color(105, 81, 133));
    r.setSeriesPaint(4, new Color(60, 141, 163));
    r.setSeriesPaint(5, new Color(204, 123, 56));
    r.setSeriesPaint(6, new Color(79, 129, 189));
    r.setSeriesPaint(7, new Color(198, 214, 172));
    r.setSeriesPaint(8, new Color(217, 170, 169));
    r.setSeriesPaint(9, new Color(247, 150, 70));
    r.setSeriesPaint(10, new Color(186, 176, 201));
    r.setSeriesPaint(11, new Color(170, 186, 215));
    r.setSeriesPaint(12, new Color(155, 187, 89));
    r.setSeriesPaint(13, new Color(169, 206, 220));
    r.setSeriesPaint(14, new Color(192, 80, 77));
    r.setSeriesPaint(15, new Color(128, 100, 162));
    r.setSeriesPaint(16, new Color(190, 115, 32));

    r.setShadowVisible(false);
    r.setBase(0.0D);

    CategoryAxis domainAxis = plot.getDomainAxis();
    domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
    domainAxis.setLowerMargin(0.0D);
    domainAxis.setUpperMargin(0.0D);
    domainAxis.setVisible(false);
    NumberAxis rangeAxis = (NumberAxis)plot.getRangeAxis();
    rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    rangeAxis.setAutoRangeIncludesZero(false);
    CombinedDomainCategoryPlot plots = 
      new CombinedDomainCategoryPlot(domainAxis);
    double value = 0.0D;
    for (int i = 0; i < this.datasetWithTotal.getColumnCount(); i++) {
      if (value < this.datasetWithTotal.getValue(0, i).doubleValue()) {
        value = this.datasetWithTotal.getValue(0, i).doubleValue();
      }
    }
    NumberAxis axis2 = new NumberAxis();
    axis2.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    axis2.setAutoRangeIncludesZero(false);
    axis2.setVisible(false);
    System.out.println(value);
    axis2.setRange(0.0D, 101.0D);
    CategoryPlot subplot1 = 
      new CategoryPlot(this.datasetWithTotal, null, 
      rangeAxis, chart.getCategoryPlot().getRenderer());
    subplot1.setDomainGridlinesVisible(false);
    CategoryDataset runrateDataset1 = 
      this.datasetWithoutTotal;
    subplot1.setRangeAxis(1, axis2);
    subplot1.setDataset(1, runrateDataset1);
    subplot1.mapDatasetToRangeAxis(1, 1);
    CategoryItemRenderer runrateRenderer1 = 
      new LineAndShapeRenderer();
    runrateRenderer1.setSeriesPaint(0, Color.RED);
    subplot1.setForegroundAlpha(0.7F);
    subplot1.setRenderer(0, chart.getCategoryPlot().getRenderer());
    subplot1.setRenderer(1, runrateRenderer1);
    plots.add(subplot1, 2);
    JFreeChart chart2 = new JFreeChart(
      title, new Font("SansSerif", 1, 12), 
      plots, true);
    chart2.removeLegend();
    chart2.setBackgroundPaint(Color.WHITE);
    return chart2;
  }

  private JFreeChart createLineChart(String title) {
    JFreeChart chart = ChartFactory.createLineChart(title, null, null, this.datasetWithoutTotal, PlotOrientation.VERTICAL, false, false, false);
    chart.setBackgroundPaint(Color.white);
    CategoryPlot plot = (CategoryPlot)chart.getPlot();
    plot.setBackgroundPaint(Color.white);
    plot.setRangeGridlinePaint(Color.black);

    LineAndShapeRenderer r = (LineAndShapeRenderer)chart.getCategoryPlot().getRenderer();

    r.setSeriesPaint(0, new Color(64, 105, 156));
    r.setSeriesPaint(1, new Color(158, 65, 62));
    r.setSeriesPaint(2, new Color(127, 154, 72));
    r.setSeriesPaint(3, new Color(105, 81, 133));
    r.setSeriesPaint(4, new Color(60, 141, 163));
    r.setSeriesPaint(5, new Color(204, 123, 56));
    r.setSeriesPaint(6, new Color(79, 129, 189));
    r.setSeriesPaint(7, new Color(198, 214, 172));
    r.setSeriesPaint(8, new Color(217, 170, 169));

    r.setDrawOutlines(true);

    BasicStroke stroke = new BasicStroke(2.0F);

    r.setSeriesStroke(0, stroke);
    r.setSeriesStroke(1, stroke);
    r.setSeriesStroke(2, stroke);
    r.setSeriesStroke(3, stroke);
    r.setSeriesStroke(4, stroke);
    r.setSeriesStroke(5, stroke);
    r.setSeriesStroke(6, stroke);
    r.setSeriesStroke(7, stroke);
    r.setSeriesStroke(8, stroke);

    CategoryAxis domainAxis = plot.getDomainAxis();
    domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
    domainAxis.setLowerMargin(0.0D);
    domainAxis.setUpperMargin(0.0D);
    domainAxis.setVisible(false);

    NumberAxis rangeAxis = (NumberAxis)plot.getRangeAxis();
    rangeAxis.setRange(0.0D, 100.0D);
    rangeAxis.setTickUnit(new NumberTickUnit(10.0D));
    rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    rangeAxis.setAutoRangeIncludesZero(true);

    return chart;
  }

  private JScrollPane getTableWithScrollPane()
  {
    JScrollPane p = new JScrollPane();
    p.setPreferredSize(this.dimensionTable);
    p.setMinimumSize(this.dimensionTable);
    p.setViewportView(this.table);
    //table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
    this.table.setCellSelectionEnabled(false);
    this.table.addMouseListener(new ChartWithTableMouseListener(this));
    return p;
  }

  private void setTableModel(boolean legend, boolean pct)
  {
    this.tableModel = new DefaultTableModel();
    ListIterator columnKeys = this.datasetWithTotal.getColumnKeys().listIterator();
    if (legend) this.tableModel.addColumn("");
    this.tableModel.addColumn("");
    while (columnKeys.hasNext()) {
      this.tableModel.addColumn(columnKeys.next());
    }

    ListIterator rowKeys = this.datasetWithTotal.getRowKeys().listIterator();
    while (rowKeys.hasNext()) {
      String tempRowElement = (String)rowKeys.next();
      Vector tempRowVector = new Vector();
      if (legend) tempRowVector.add("");
      tempRowVector.add(tempRowElement);
      columnKeys = this.datasetWithTotal.getColumnKeys().listIterator();
      while (columnKeys.hasNext()) {
        String temp = "";
        try { temp = this.datasetWithTotal.getValue(tempRowElement, (Comparable)columnKeys.next()).toString(); } catch (Exception e) { temp = ""; }

        if (temp.isEmpty()) {
          temp = "0";
        }
        if (pct) tempRowVector.add(this.bf.toProcent(Double.parseDouble(temp) / 100.0D, false));
        if (!pct) tempRowVector.add(this.bf.KiloDotFill((int)Double.parseDouble(temp), false));
      }
      this.tableModel.addRow(tempRowVector);
    }
    this.table.setModel(this.tableModel);
    this.table.setDefaultRenderer(Object.class, new ChartTableRenderer(legend));
    this.chartPanel.addMouseListener(new ChartWithTableMouseListener(this));
    this.table.getTableHeader().setDefaultRenderer(new ChartTableHeaderRenderer(legend, this));
    this.table.getTableHeader().setPreferredSize(new Dimension(this.table.getColumnModel().getTotalColumnWidth(), 50));

    this.table.getTableHeader().setFont(new Font("Arial", 0, 9));
    if (legend) {
      this.table.getColumnModel().getColumn(0).setPreferredWidth(5);
      this.table.getColumnModel().getColumn(0).setMaxWidth(5);
      this.table.getColumnModel().getColumn(1).setPreferredWidth(115);
      this.table.getColumnModel().getColumn(1).setMinWidth(115);
      this.table.getColumnModel().getColumn(1).setMaxWidth(115);
    } else {
      this.table.getColumnModel().getColumn(0).setPreferredWidth(113);
      this.table.getColumnModel().getColumn(0).setMinWidth(113);
      this.table.getColumnModel().getColumn(0).setMaxWidth(113);
    }
  }

  public void setNewMailData(char c, Vector<CallQueue> data)
  {
    this.datasetWithoutTotal = createDataSetNewEmailData(data, false);
    this.datasetWithTotal = createDataSetNewEmailData(data, true);
    String periode;
    switch (c) { case 'D':
      periode = "Besvarelsesprocenter i KS"; break;
    case 'W':
      periode = "Besvarelsesprocenter i KS"; break;
    case 'M':
      periode = "Besvarelsesprocenter i KS"; break;
    case 'Y':
      periode = "Besvarelsesprocenter i KS"; break;
    default:
      return;
    }
    String title = periode;
    this.chartPanel.setPreferredSize(this.dimensionChart);

    setTableModel(true, true);
  }
  
  public void setCallbacksData(char c, Vector<CallQueue> data)
  {
    this.datasetWithTotal = createDataSetCallbacks(data, true);
    String periode;
    switch (c) { case 'D':
      periode = "Callbacks i KS"; break;
    case 'W':
      periode = "Callbacks i KS"; break;
    case 'M':
      periode = "Callbacks i KS"; break;
    case 'Y':
      periode = "Callbacks i KS"; break;
    default:
      return;
    }
    String title = periode;
    this.chart = createDualAxisChart(title);
    this.table.setAlignmentX(20.0F);
    this.chart.setPadding(new RectangleInsets(0.0D, 0.0D, 0.0D, 0.0D));
    this.chartPanel.setPreferredSize(new Dimension(100, 140));
    this.chartPanel.setChart(this.chart);
    setTableModel(false, false, this.createDataSetCallBacksLine(data, true));
      } 
  
  private void setTableModel(boolean legend, boolean pct, DefaultCategoryDataset datasetWithoutTotal2) {
    this.tableModel = new DefaultTableModel();

    ListIterator columnKeys = this.datasetWithTotal.getColumnKeys().listIterator();
    if (legend) this.tableModel.addColumn("");

    this.tableModel.addColumn("");
    int j = 0;
    while (columnKeys.hasNext()) {
      if (j == 15) {
        break;
      }
      this.tableModel.addColumn(columnKeys.next());

      j++;
    }
    ListIterator rowKeys = this.datasetWithTotal.getRowKeys().listIterator();
    while (rowKeys.hasNext()) {
      String tempRowElement = (String)rowKeys.next();
      Vector tempRowVector = new Vector();
      if (legend) tempRowVector.add("");
      tempRowVector.add(tempRowElement);

      columnKeys = this.datasetWithTotal.getColumnKeys().listIterator();
      int i = 0;
      while (columnKeys.hasNext()) {
        String temp = "";
        try { temp = this.datasetWithTotal.getValue(tempRowElement, (Comparable)columnKeys.next()).toString(); } catch (Exception e) { temp = ""; }

        if (temp.isEmpty()) {
          temp = "0";
        }
        if (pct) tempRowVector.add(this.bf.toProcent(Double.parseDouble(temp) / 100.0D, false));
        if (!pct) tempRowVector.add(this.bf.KiloDotFill((int)Double.parseDouble(temp), false));
      }
      this.tableModel.addRow(tempRowVector);
    }
    addAdditonalDataToTabel(datasetWithoutTotal2, true);
    this.table.setModel(this.tableModel);
    this.table.setDefaultRenderer(Object.class, new ChartTableRenderer(legend));
    this.chartPanel.addMouseListener(new ChartWithTableMouseListener(this));
    this.table.getTableHeader().setDefaultRenderer(new ChartTableHeaderRenderer(legend, this));
    this.table.getTableHeader().setPreferredSize(new Dimension(this.table.getColumnModel().getTotalColumnWidth(), 50));

    this.table.getTableHeader().setFont(new Font("Arial", 0, 9));

    if (legend) {
      this.table.getColumnModel().getColumn(0).setPreferredWidth(5);
      this.table.getColumnModel().getColumn(0).setMaxWidth(5);
      this.table.getColumnModel().getColumn(1).setPreferredWidth(115);
      this.table.getColumnModel().getColumn(1).setMinWidth(115);
      this.table.getColumnModel().getColumn(1).setMaxWidth(115);
    } else {
      this.table.getColumnModel().getColumn(0).setPreferredWidth(113);
      this.table.getColumnModel().getColumn(0).setMinWidth(113);
      this.table.getColumnModel().getColumn(0).setMaxWidth(113);
    }
  }

  private void addAdditonalDataToTabel(DefaultCategoryDataset dataSetToBeAdded, boolean pct)
  {
    ListIterator rowKeys = dataSetToBeAdded.getRowKeys().listIterator();
    int i = 0;
    while (rowKeys.hasNext()) {
      if (i == 14) {
        break;
      }
      i++;
      String tempRowElement = (String)rowKeys.next();
      Vector tempRowVector = new Vector();
      tempRowVector.add(tempRowElement);
      ListIterator columnKeys = dataSetToBeAdded.getColumnKeys().listIterator();
      while (columnKeys.hasNext()) {
        String temp = "";
        try { temp = dataSetToBeAdded.getValue(tempRowElement, (Comparable)columnKeys.next()).toString(); } catch (Exception e) { temp = ""; }

        if (temp.isEmpty()) {
          temp = "0";
        }
        if (pct) tempRowVector.add(this.bf.toProcent(Double.parseDouble(temp) / 100.0D, false));
        if (!pct) tempRowVector.add(this.bf.KiloDotFill((int)Double.parseDouble(temp), false));
      }
      this.tableModel.addRow(tempRowVector);
    }
  }

  private DefaultCategoryDataset createDataSetCallBacksLine(Vector<CallQueue> data, boolean b)
  {
    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    for (CallQueue callQueue : data) {
      dataset.addValue(callQueue.getCallbackBesvarelsesPct() * 100.0D, "Inden for 2 timer", callQueue.getPeriod());
    }
    return dataset;
  }

  private DefaultCategoryDataset createDataSetCallbacks(Vector<CallQueue> data, boolean b)
  {
    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    for (CallQueue callQueue : data) {
      dataset.addValue(callQueue.getCallback().getCalls(), "Behandlede Callbacks", callQueue.getPeriod());
    }
    return dataset;
  }
}
