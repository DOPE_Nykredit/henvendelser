public class CallType
{
  private double calls;
  private double answeredCalls;
  private double answeredCalls25Seconds;
  private double internalCalls;
  private double exceeded;
  
  public CallType(double calls, double answeredCalls, double answeredCalls25Seconds, double internalCalls, double exceeded)
  {
    this.calls = calls;
    this.answeredCalls = answeredCalls;
    this.answeredCalls25Seconds = answeredCalls25Seconds;
    this.internalCalls = internalCalls;
    this.exceeded = exceeded;
  }
  public double getCalls() {
    return this.calls; } 
  public void setHenvendelser(double calls) { this.calls = calls; } 
  public double getAnsweredCalls() {
    return this.answeredCalls; } 
  public void setAnsweredCalls(double answeredCalls) { this.answeredCalls = answeredCalls; } 
  public double getAnsweredCalls25Seconds() {
    return this.answeredCalls25Seconds;
  }
  public void setAnsweredCalls25Seconds(double answeredCalls25Seconds) { this.answeredCalls25Seconds = answeredCalls25Seconds; }

  public double getInternalCalls() {
    return this.internalCalls; } 
  public void setInternalCalls(double internalCalls) { this.internalCalls = internalCalls; } 
  public double getExceeded() {
    return this.exceeded; } 
  public void setExceeded(double exceeded) { this.exceeded = exceeded; }

}