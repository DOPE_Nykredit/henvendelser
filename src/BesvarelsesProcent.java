
public class BesvarelsesProcent {

	private int totalAntal;
	private int besvaret;
	
	public BesvarelsesProcent(int totalAntal, int besvaret){
		this.totalAntal = totalAntal;
		this.besvaret = besvaret;
	}
	
	public double getResult(){
		return (double) this.besvaret / (double) this.totalAntal;
	}
	public double getResultPct(){	
		return this.getResult() * 100;
	}

	@Override
	public String toString(){
		return Math.round(this.getResultPct()) + "%";
	}
}
