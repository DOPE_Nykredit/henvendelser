public class CallQueue
{
  private String period;
  private CallType webdeskPercentageOfTotalAnswered = new CallType(0.0, 0.0, 0.0D, 0.0, 0.0);
  private CallType supportOpgaver = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType otbOpgaver = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType hovednumre = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType s70151600 = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType boligejerneshus = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType privatcentre = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType forsikring = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType hotline = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType tekniskhotline = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType pensionhotline = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType stabe = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType kunder = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType erhverv = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType erhvervstorkunder =new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType erhvervoverflow = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType basisintern = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType boligintern = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType investeringpension = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType kredit = new CallType(0.0, 0.0, 0.0, 0.0, 0.0); 
  private CallType forsikringintern = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType totalkredit = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType vipservice = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType vipcenterkald = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType datafejl = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType postogfax = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType forsikringsopgaver = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType webdesk = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType personligekort = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType emailIndgående = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType emailB9 = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType emailBehandlede = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType emailKampagne = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType nykreditKunder = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType callBack = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);
  private CallType webdesk2013 = new CallType(0.0, 0.0, 0.0, 0.0, 0.0);

  public CallQueue(String period) {
    this.period = period;
  }
  public void setSupportOpgaver(CallType supportObject) {
    this.supportOpgaver = supportObject;
  }
  
  public void setOTBOpgaver(CallType otbObject) {
	    this.otbOpgaver = otbObject;
	  }
  public void setWebdeskPercentageOfTotalAnswered(CallType webdeskPercentageOfTotalAnswered) {
    this.webdeskPercentageOfTotalAnswered = webdeskPercentageOfTotalAnswered;
  }
  public CallType getCallback() {
    return this.callBack;
  }

  public String getPeriod() {
    return this.period;
  }
  public void setPeriod(String period) {
    this.period = period;
  }
  public CallType getHovednumre() {
    return this.hovednumre;
  }
  public void setHovednumre(CallType hovednumre) {
    this.hovednumre = hovednumre;
  }
  public CallType getNykreditKunder() {
    return this.nykreditKunder;
  }
  public void setNykreditKunder(CallType nykreditKunder) {
    this.nykreditKunder = nykreditKunder;
  }
  public CallType getS70151600() {
    return this.s70151600;
  }
  public void setS70151600(CallType s70151600) {
    this.s70151600 = s70151600;
  }
  public CallType getBoligejerneshus() {
    return this.boligejerneshus;
  }
  public void setBoligejerneshus(CallType boligejerneshus) {
    this.boligejerneshus = boligejerneshus;
  }
  public CallType getPrivatcentre() {
    return this.privatcentre;
  }
  public void setPrivatcentre(CallType privatcentre) {
    this.privatcentre = privatcentre;
  }
  public CallType getForsikring() {
    return this.forsikring;
  }
  public void setForsikring(CallType forsikring) {
    this.forsikring = forsikring;
  }
  public CallType getHotline() {
    return this.hotline;
  }
  public void setHotline(CallType hotline) {
    this.hotline = hotline;
  }
  public CallType getTekniskhotline() {
    return this.tekniskhotline;
  }
  public void setTekniskhotline(CallType tekniskhotline) {
    this.tekniskhotline = tekniskhotline;
  }
  public CallType getPensionhotline() {
    return this.pensionhotline;
  }
  public void setPensionhotline(CallType pensionhotline) {
    this.pensionhotline = pensionhotline;
  }
  public CallType getStabe() {
    return this.stabe;
  }
  public void setStabe(CallType stabe) {
    this.stabe = stabe;
  }
  public CallType getKunder() {
    return this.kunder;
  }
  public void setKunder(CallType kunder) {
    this.kunder = kunder;
  }
  public CallType getErhverv() {
    return this.erhverv;
  }
  public void setErhverv(CallType erhverv) {
    this.erhverv = erhverv;
  }
  public CallType getErhvervstorkunder() {
    return this.erhvervstorkunder;
  }
  public void setErhvervstorkunder(CallType erhvervstorkunder) {
    this.erhvervstorkunder = erhvervstorkunder;
  }
  public CallType getErhvervoverflow() {
    return this.erhvervoverflow;
  }
  public void setErhvervoverflow(CallType erhvervoverflow) {
    this.erhvervoverflow = erhvervoverflow;
  }
  public CallType getSupportOpgaver() {
    return this.supportOpgaver;
  }
  public CallType getOTBOpgaver() {
	    return this.otbOpgaver;
	  }  
  public CallType getBasisintern() {
    return this.basisintern;
  }
  public void setBasisintern(CallType basisintern) {
    this.basisintern = basisintern;
  }
  public CallType getBoligintern() {
    return this.boligintern;
  }
  public void setBoligintern(CallType boligintern) {
    this.boligintern = boligintern;
  }
  public CallType getInvesteringpension() {
    return this.investeringpension;
  }
  public void setInvesteringpension(CallType investeringpension) {
    this.investeringpension = investeringpension;
  }
  public CallType getKredit() {
    return this.kredit;
  }
  public CallType getwebdeskPercentageOfTotalAnswered() {
    return this.webdeskPercentageOfTotalAnswered;
  }
  public void setKredit(CallType kredit) {
    this.kredit = kredit;
  }
  public CallType getForsikringintern() {
    return this.forsikringintern;
  }
  public void setForsikringintern(CallType forsikringintern) {
    this.forsikringintern = forsikringintern;
  }
  public CallType getTotalkredit() {
    return this.totalkredit;
  }
  public void setTotalkredit(CallType totalkredit) {
    this.totalkredit = totalkredit;
  }
  public CallType getVipservice() {
    return this.vipservice;
  }
  public void setVipservice(CallType vipservice) {
    this.vipservice = vipservice;
  }
  public CallType getVipcenterkald() {
    return this.vipcenterkald;
  }
  public void setVipcenterkald(CallType vipcenterkald) {
    this.vipcenterkald = vipcenterkald;
  }
  public CallType getDatafejl() {
    return this.datafejl;
  }
  public void setDatafejl(CallType datafejl) {
    this.datafejl = datafejl;
  }
  public CallType getPostogfax() {
    return this.postogfax;
  }
  public void setPostogfax(CallType postogfax) {
    this.postogfax = postogfax;
  }
  public CallType getForsikringsopgaver() {
    return this.forsikringsopgaver;
  }
  public void setForsikringsopgaver(CallType forsikringsopgaver) {
    this.forsikringsopgaver = forsikringsopgaver;
  }
  public CallType getWebdesk() {
    return this.webdesk;
  }
  public void setWebdesk(CallType webdesk) {
    this.webdesk = webdesk;
  }
  public CallType getPersonligekort() {
    return this.personligekort;
  }
  public CallType getWebdesk2013() {
    return this.webdesk2013;
  }
  public void setWebdesk2013(CallType webdesk2013) {
    this.webdesk2013 = webdesk2013;
  }
  public void setPersonligekort(CallType personligekort) {
    this.personligekort = personligekort;
  }
  public CallType getEmailIndgående() {
    return this.emailIndgående;
  }
  public void setEmailIndgående(CallType emailIndgående) {
    this.emailIndgående = emailIndgående;
  }
  public CallType getEmailB9() {
    return this.emailB9;
  }
  public void setEmailB9(CallType emailB9) {
    this.emailB9 = emailB9;
  }
  public CallType getEmailBehandlede() {
    return this.emailBehandlede;
  }
  public void setEmailBehandlede(CallType emailBehandlede) {
    this.emailBehandlede = emailBehandlede;
  }

  public CallType getEmailKampagne() {
    return this.emailKampagne;
  }

  public void setEmailKampagne(CallType emailKampagne) {
    this.emailKampagne = emailKampagne;
  }

  public double getCallbackBesvarelsesPct()
  {
    return this.callBack.getAnsweredCalls25Seconds() / this.callBack.getCalls();
  }
 
  public double getTotalKreditBesvarelsesPct25()
  {
	    double g = this.totalkredit.getAnsweredCalls25Seconds() / this.totalkredit.getCalls() * 100.0D;
	    if (g < 0.0D) {
	      g *= -1.0D;
	      return g;
	    }

	    return g;
	  }
    
  public double getHovednumreBesvarelsesPct25() {
    return 
      (this.hovednumre.getAnsweredCalls25Seconds() + this.s70151600.getAnsweredCalls25Seconds()) / (
      this.hovednumre.getCalls() + this.s70151600.getCalls());
  }

  public double getPrivatcentreBesvarelsesPct25() {
    return 
      (this.boligejerneshus.getAnsweredCalls25Seconds() + this.privatcentre.getAnsweredCalls25Seconds()) / (
      this.boligejerneshus.getCalls() + this.privatcentre.getCalls());
  }

  public double getForsikringBesvarelsesPct25() {
    return 
      this.forsikring.getAnsweredCalls25Seconds() / 
      this.forsikring.getCalls();
  }

  public double getHotlineBesvarelsesPct25() {
    return 
      (this.hotline.getAnsweredCalls25Seconds() + this.tekniskhotline.getAnsweredCalls25Seconds()) / (
      this.hotline.getCalls() + this.tekniskhotline.getCalls());
  }

  public double getStabeBesvarelsesPct25() {
    return this.stabe.getAnsweredCalls25Seconds() / this.stabe.getCalls();
  }

  public double getKunderBesvarelsesPct25() {
    return this.kunder.getAnsweredCalls25Seconds() / this.kunder.getCalls();
  }

  public double getErhvervBesvarelsesPct25() {
    if (this.erhverv.getAnsweredCalls25Seconds() + this.erhvervstorkunder.getAnsweredCalls25Seconds() + this.erhvervoverflow.getAnsweredCalls25Seconds() == 0.0D) {
      return 0.0D;
    }
    return (this.erhverv.getAnsweredCalls25Seconds() + this.erhvervstorkunder.getAnsweredCalls25Seconds() + this.erhvervoverflow.getAnsweredCalls25Seconds()) / (this.erhverv.getCalls() + this.erhvervstorkunder.getCalls() + this.erhvervoverflow.getCalls());
  }
  public double getWebdesk2013percentage() {
    return this.webdesk2013.getAnsweredCalls() / this.webdesk2013.getCalls();
  }

  public double getVipServiceBesvarelsesPct25() {
    if (this.vipservice.getAnsweredCalls() + this.vipcenterkald.getAnsweredCalls25Seconds() == 0.0D) {
      return 0.0D;
    }
    return (this.vipservice.getAnsweredCalls25Seconds() + this.vipcenterkald.getAnsweredCalls25Seconds()) / (this.vipservice.getCalls() + this.vipcenterkald.getCalls());
  }

  public double getWebdeskPercentageOfTotalAnswered() {
    double g = this.webdeskPercentageOfTotalAnswered.getAnsweredCalls25Seconds() / this.webdeskPercentageOfTotalAnswered.getCalls() * 100.0D;
    if (g < 0.0D) {
      g *= -1.0D;
      return g;
    }

    return g;
  }

  public double getKundeserviceBesvarelsesPct25()
  {
    double number = getKundeserviceCallOnlyTotal().getAnsweredCalls25Seconds() / getKundeserviceCallOnlyTotal().getCalls();
    return number;
  }

  public double getKundeserviceBesvarelsesPct() {
    return getKundeserviceCallOnlyTotal().getAnsweredCalls() / getKundeserviceCallOnlyTotal().getCalls();
  }

  public double getStabeBesvarelsesPct() {
    return this.stabe.getAnsweredCalls() / this.stabe.getCalls();
  }

  public double getEmailStatBesvarelsesPct() {
    return (getEmailStatIalt() - getEmailStatIaltOverskredet()) / getEmailStatIalt();
  }

  public double getEmailStatIalt() {
    return this.emailB9.getCalls() + this.emailBehandlede.getCalls();
  }

  public double getEmailStatIaltOverskredet() {
    return this.emailB9.getExceeded() + this.emailBehandlede.getExceeded();
  }

  public double getWebdeskBesvarelsesPct() {
    return this.webdesk.getAnsweredCalls() / this.webdesk.getCalls();
  }

  public double getB9BesvarelsesPct() {
    return (this.emailB9.getCalls() - this.emailB9.getExceeded()) / this.emailB9.getCalls();
  }

  public CallType getKundeserviceCallOnlyTotal() {
    double totalCalls = 
      this.hovednumre.getCalls() + 					/* Hovednumre */
      this.s70151600.getCalls() + 					/* Hovednumre */
      this.boligejerneshus.getCalls() + 			/* Privatcentre */
      this.privatcentre.getCalls() + 				/* Privatcentre */
      this.forsikring.getCalls() + 					/* Forsikring */
      this.hotline.getCalls() + 					/* Hotline + TekniskHotline */
      this.tekniskhotline.getCalls() + 				/* Hotline + TekniskHotline */
      this.pensionhotline.getCalls() + 				/* PensionsHotline */
      this.stabe.getCalls() + 						/* Stabe */
      this.kunder.getCalls() + 						/* Kunder */
      this.erhverv.getCalls() + 					/* Erhverv */
      this.erhvervstorkunder.getCalls() + 			/* Erhverv */
      this.erhvervoverflow.getCalls() + 			/* Erhverv */
      this.basisintern.getCalls() + 				/* Ekst 2nd level */
      this.boligintern.getCalls() + 				/* Ekst 2nd level */
      this.investeringpension.getCalls() + 			/* Ekst 2nd level */
      this.kredit.getCalls() + 						/* Ekst 2nd level */
      this.forsikringintern.getCalls() + 			/* Ekst 2nd level */
      this.totalkredit.getCalls() + 				/* Totalkredit */
      this.vipservice.getCalls() + 					/* VIP + VIP Center */
      this.vipcenterkald.getCalls() + 				/* VIP + VIP Center */
      this.nykreditKunder.getCalls() + 				/* NykreditKunder */
      this.datafejl.getCalls();						/* Datafejl (Tjek og se om den hægter sig på antal kald) */

    /* 23 kun kald */
    
    double answeredCalls = 
      this.hovednumre.getAnsweredCalls() + 
      this.s70151600.getAnsweredCalls() + 
      this.boligejerneshus.getAnsweredCalls() + 
      this.privatcentre.getAnsweredCalls() + 
      this.forsikring.getAnsweredCalls() + 
      this.hotline.getAnsweredCalls() + 
      this.tekniskhotline.getAnsweredCalls() + 
      this.pensionhotline.getAnsweredCalls() + 
      this.stabe.getAnsweredCalls() + 
      this.kunder.getAnsweredCalls() + 
      this.erhverv.getAnsweredCalls() + 
      this.erhvervstorkunder.getAnsweredCalls() + 
      this.erhvervoverflow.getAnsweredCalls() + 
      this.basisintern.getAnsweredCalls() + 
      this.boligintern.getAnsweredCalls() + 
      this.investeringpension.getAnsweredCalls() + 
      this.kredit.getAnsweredCalls() + 
      this.forsikringintern.getAnsweredCalls() + 
      this.totalkredit.getAnsweredCalls() + 
      this.vipservice.getAnsweredCalls() + 
      this.vipcenterkald.getAnsweredCalls() + 
      this.nykreditKunder.getAnsweredCalls() + 
      this.datafejl.getAnsweredCalls();

    double answeredCalls25Seconds = 
      this.hovednumre.getAnsweredCalls25Seconds() + 
      this.s70151600.getAnsweredCalls25Seconds() + 
      this.boligejerneshus.getAnsweredCalls25Seconds() + 
      this.privatcentre.getAnsweredCalls25Seconds() + 
      this.forsikring.getAnsweredCalls25Seconds() + 
      this.hotline.getAnsweredCalls25Seconds() + 
      this.tekniskhotline.getAnsweredCalls25Seconds() + 
      this.pensionhotline.getAnsweredCalls25Seconds() + 
      this.stabe.getAnsweredCalls25Seconds() + 
      this.kunder.getAnsweredCalls25Seconds() + 
      this.erhverv.getAnsweredCalls25Seconds() + 
      this.erhvervstorkunder.getAnsweredCalls25Seconds() + 
      this.erhvervoverflow.getAnsweredCalls25Seconds() + 
      this.basisintern.getAnsweredCalls25Seconds() + 
      this.boligintern.getAnsweredCalls25Seconds() + 
      this.investeringpension.getAnsweredCalls25Seconds() + 
      this.kredit.getAnsweredCalls25Seconds() + 
      this.forsikringintern.getAnsweredCalls25Seconds() + 
      this.totalkredit.getAnsweredCalls25Seconds() + 
      this.vipservice.getAnsweredCalls25Seconds() + 
      this.vipcenterkald.getAnsweredCalls25Seconds() + 
      this.nykreditKunder.getAnsweredCalls25Seconds() + 
      this.datafejl.getAnsweredCalls25Seconds();

    double internalCalls = 
      this.hovednumre.getInternalCalls() + 
      this.s70151600.getInternalCalls() + 
      this.boligejerneshus.getInternalCalls() + 
      this.privatcentre.getInternalCalls() + 
      this.forsikring.getInternalCalls() + 
      this.hotline.getInternalCalls() + 
      this.tekniskhotline.getInternalCalls() + 
      this.pensionhotline.getInternalCalls() + 
      this.stabe.getInternalCalls() + 
      this.kunder.getInternalCalls() + 
      this.erhverv.getInternalCalls() + 
      this.erhvervstorkunder.getInternalCalls() + 
      this.erhvervoverflow.getInternalCalls() + 
      this.basisintern.getInternalCalls() + 
      this.boligintern.getInternalCalls() + 
      this.investeringpension.getInternalCalls() + 
      this.kredit.getInternalCalls() + 
      this.forsikringintern.getInternalCalls() + 
      this.totalkredit.getInternalCalls() + 
      this.vipservice.getInternalCalls() + 
      this.vipcenterkald.getInternalCalls() +
      this.nykreditKunder.getInternalCalls() +
      this.datafejl.getInternalCalls();

    return new CallType(totalCalls, answeredCalls, answeredCalls25Seconds, internalCalls, 0.0);
  }

  public CallType getKundeserviceTotal() {
    double totalCalls = 
      this.hovednumre.getCalls() + 
      this.s70151600.getCalls() + 
      this.boligejerneshus.getCalls() + 
      this.privatcentre.getCalls() + 
      this.forsikring.getCalls() + 
      this.hotline.getCalls() + 
      this.tekniskhotline.getCalls() + 
      this.pensionhotline.getCalls() + 
      this.stabe.getCalls() + 
      this.kunder.getCalls() + 
      this.erhverv.getCalls() + 
      this.erhvervstorkunder.getCalls() + 
      this.erhvervoverflow.getCalls() + 
      this.basisintern.getCalls() + 
      this.boligintern.getCalls() + 
      this.investeringpension.getCalls() + 
      this.kredit.getCalls() + 
      this.forsikringintern.getCalls() + 
      this.totalkredit.getCalls() +
      this.vipservice.getCalls() +
      this.vipcenterkald.getCalls() +
      //this.datafejl.getCalls() +
      //this.postogfax.getCalls() +
      //this.forsikringsopgaver.getCalls() +
      this.webdesk.getCalls() +
     // this.personligekort.getCalls() +
      this.nykreditKunder.getCalls() + 
      this.supportOpgaver.getCalls() + 
      this.otbOpgaver.getCalls() +
      this.emailIndgående.getCalls();
    
    /* 30 I alt */

    double answeredCalls = 
      this.hovednumre.getAnsweredCalls() + 
      this.s70151600.getAnsweredCalls() + 
      this.boligejerneshus.getAnsweredCalls() + 
      this.privatcentre.getAnsweredCalls() + 
      this.forsikring.getAnsweredCalls() + 
      this.hotline.getAnsweredCalls() + 
      this.tekniskhotline.getAnsweredCalls() + 
      this.pensionhotline.getAnsweredCalls() + 
      this.stabe.getAnsweredCalls() + 
      this.kunder.getAnsweredCalls() + 
      this.erhverv.getAnsweredCalls() + 
      this.erhvervstorkunder.getAnsweredCalls() + 
      this.erhvervoverflow.getAnsweredCalls() + 
      this.basisintern.getAnsweredCalls() + 
      this.boligintern.getAnsweredCalls() + 
      this.investeringpension.getAnsweredCalls() + 
      this.kredit.getAnsweredCalls() + 
      this.forsikringintern.getAnsweredCalls() + 
      this.totalkredit.getAnsweredCalls() + 
      this.vipservice.getAnsweredCalls() + 
      this.vipcenterkald.getAnsweredCalls() + 
      this.datafejl.getAnsweredCalls() + 
      this.postogfax.getAnsweredCalls() + 
      this.forsikringsopgaver.getAnsweredCalls() + 
      this.webdesk.getAnsweredCalls() + 
      this.personligekort.getAnsweredCalls() + 
      this.nykreditKunder.getAnsweredCalls() + 
      this.supportOpgaver.getAnsweredCalls() +
      this.otbOpgaver.getAnsweredCalls() +
      this.emailIndgående.getAnsweredCalls();

/* 30 i alt */    
    
    double answeredCalls25Seconds = 
      this.hovednumre.getAnsweredCalls25Seconds() + 
      this.s70151600.getAnsweredCalls25Seconds() + 
      this.boligejerneshus.getAnsweredCalls25Seconds() + 
      this.privatcentre.getAnsweredCalls25Seconds() + 
      this.forsikring.getAnsweredCalls25Seconds() + 
      this.hotline.getAnsweredCalls25Seconds() + 
      this.tekniskhotline.getAnsweredCalls25Seconds() + 
      this.pensionhotline.getAnsweredCalls25Seconds() + 
      this.stabe.getAnsweredCalls25Seconds() + 
      this.kunder.getAnsweredCalls25Seconds() + 
      this.erhverv.getAnsweredCalls25Seconds() + 
      this.erhvervstorkunder.getAnsweredCalls25Seconds() + 
      this.erhvervoverflow.getAnsweredCalls25Seconds() + 
      this.basisintern.getAnsweredCalls25Seconds() + 
      this.boligintern.getAnsweredCalls25Seconds() + 
      this.investeringpension.getAnsweredCalls25Seconds() + 
      this.kredit.getAnsweredCalls25Seconds() + 
      this.forsikringintern.getAnsweredCalls25Seconds() + 
      this.totalkredit.getAnsweredCalls25Seconds() + 
      this.vipservice.getAnsweredCalls25Seconds() + 
      this.vipcenterkald.getAnsweredCalls25Seconds() + 
      this.datafejl.getAnsweredCalls25Seconds() + 
      this.postogfax.getAnsweredCalls25Seconds() + 
      this.forsikringsopgaver.getAnsweredCalls25Seconds() + 
      this.webdesk.getAnsweredCalls25Seconds() + 
      this.personligekort.getAnsweredCalls25Seconds() + 
      this.nykreditKunder.getAnsweredCalls25Seconds() +
      this.supportOpgaver.getAnsweredCalls25Seconds() +
      this.otbOpgaver.getAnsweredCalls25Seconds() +
      this.emailIndgående.getAnsweredCalls25Seconds();

    /* 30 i alt */
    
    double internalCalls = 
      this.hovednumre.getInternalCalls() + 
      this.s70151600.getInternalCalls() + 
      this.boligejerneshus.getInternalCalls() + 
      this.privatcentre.getInternalCalls() + 
      this.forsikring.getInternalCalls() + 
      this.hotline.getInternalCalls() + 
      this.tekniskhotline.getInternalCalls() + 
      this.pensionhotline.getInternalCalls() + 
      this.stabe.getInternalCalls() + 
      this.kunder.getInternalCalls() + 
      this.erhverv.getInternalCalls() + 
      this.erhvervstorkunder.getInternalCalls() + 
      this.erhvervoverflow.getInternalCalls() + 
      this.basisintern.getInternalCalls() + 
      this.boligintern.getInternalCalls() + 
      this.investeringpension.getInternalCalls() + 
      this.kredit.getInternalCalls() + 
      this.forsikringintern.getInternalCalls() + 
      this.totalkredit.getInternalCalls() + 
      this.vipservice.getInternalCalls() + 
      this.vipcenterkald.getInternalCalls() + 
      this.datafejl.getInternalCalls() + 
      this.postogfax.getInternalCalls() + 
      this.forsikringsopgaver.getInternalCalls() + 
      this.webdesk.getInternalCalls() + 
      this.personligekort.getInternalCalls() +
      this.nykreditKunder.getInternalCalls() +
      this.supportOpgaver.getInternalCalls() +
      this.otbOpgaver.getInternalCalls() +
      this.emailIndgående.getInternalCalls();

    return new CallType(totalCalls, answeredCalls, answeredCalls25Seconds, internalCalls, 0.0);
  }
  public void setCallback(CallType callType) {
    this.callBack = callType;
  }
}