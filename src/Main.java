import com.nykredit.kundeservice.swing.DateSelector;
import com.nykredit.kundeservice.swing.NFrame;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;

public class Main extends NFrame
{
  private static final long serialVersionUID = 1L;
  private ChartWithTable chartHenvendelser;
  private ChartWithTable5 chartCallbacks;
  private ChartWithTable6 chartEmailHenvendelser;
  private ChartWithTable4 chartEmailData;
  private ChartWithTable3 chartInterneHenvendelser;
  private ChartWithTable2 chartBesvarelsesPct25Sek;
  private ChartWithTable8 telefoniskBesvarelsesprocent;
  private ChartWithTable7 chartEmailBesvarelsesPct;
  public static String startDate = "";
  public static String endDate = "";
  private JButton buttonGetData;
  private EOracle eOracle;
  private DateSelector dateSelector;
  private JTabbedPane tabs;

  private JPanel getPanel()
  {
    JPanel p = new JPanel(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.gridy = 0;
    c.insets = new Insets(5, 5, 0, 0);
    p.add(getControlPanel(), c);

    c.weightx = 1.0D;
    c.weighty = 1.0D;
    c.fill = 1;
    c.gridy = 1;
    c.gridwidth = 2;
    c.insets = new Insets(5, 5, 0, 5);
    p.add(getTabs(), c);

    c.weightx = 0.0D;
    c.weighty = 0.0D;
    c.fill = 0;
    c.anchor = 14;
    c.gridwidth = 1;
    c.gridy = 2;
    c.gridx = 1;
    c.gridwidth = 1;
    c.insets = new Insets(5, 5, 5, 10);
    p.add(new JLabel("DOPE � Nykredit"), c);
    return p;
  }

  private JPanel getControlPanel() {
    JPanel p = new JPanel(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    p.add(getDateSelector(), c);

    c.insets = new Insets(0, 5, 0, 0);
    c.fill = 3;
    p.add(getButtonGetData(), c);

    return p;
  }

  private DateSelector getDateSelector() {
    this.dateSelector = new DateSelector();
    this.dateSelector.setMinimumSize(new Dimension(200, 150));
    JComboBox tempCombo = this.dateSelector.getPeriodComboBox();
    tempCombo.removeItem("Ad-hoc");
    this.dateSelector.setPeriodComboBox(tempCombo);
    return this.dateSelector;
  }

  private JButton getButtonGetData() {
    this.buttonGetData = new JButton("Hent data");
    this.buttonGetData.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        Main.this.fetchData();
      }
    });
    return this.buttonGetData;
  }

  private void fetchData()
  {
    startDate = this.dateSelector.getStart();
    endDate = this.dateSelector.getEnd();
    char c = this.dateSelector.getValg();

    Vector data = null;
    this.eOracle.SS_spy();
    data = this.eOracle.getCallQueueArray(startDate, endDate, c);
    data = this.eOracle.getQueueReport(startDate, endDate, c, data);
    data = this.eOracle.getWebdesk(startDate, endDate, c, data);
    data = this.eOracle.getWebdesk2013(startDate, endDate, c, data);
    data = this.eOracle.getPostAndFax(startDate, endDate, c, data);
    data = this.eOracle.getSupportOpgaver(startDate, endDate, c, data);
    data = this.eOracle.getOTBOpgaver(startDate, endDate, c, data);
    data = this.eOracle.getEmail(startDate, endDate, c, data);
    data = this.eOracle.getCallbacks(startDate, endDate, c, data);
    data = this.eOracle.getCallbacksCompletedDevision(startDate, endDate, c, data);
    this.chartCallbacks.setCallbacksData(c, data);
    this.chartHenvendelser.setHenvendelser(c, data);
    this.chartEmailHenvendelser.setEmailHenvendelser(c, data);
    this.chartInterneHenvendelser.setInterneHenvendelser(c, data);
    this.chartBesvarelsesPct25Sek.setBesvarelsesProcenter25Sec(c, data);
    this.telefoniskBesvarelsesprocent.setBesvarelsesProcenter(c, data);
    this.chartEmailBesvarelsesPct.setEmailBesvarelsesProcenter(c, data);
    this.chartEmailData.setEmailData(c, data);
  }

  public static void main(String[] args)
  {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } catch (Throwable e) {
      e.printStackTrace();
    }
    new Main("Henvendelser v3.1");
  }

  public Main(String program)
  {
    this.eOracle = new EOracle(program);
    setContentPane(getPanel());
    setTitle(program);
    pack();
    setSize(678, 872);
    setMinimumSize(new Dimension(getWidth(), getHeight()));
    setLocationRelativeTo(getRootPane());
    setResizable(false);
    fetchData();
    setVisible(true);
  }

  private JTabbedPane getTabs() {
    this.tabs = new JTabbedPane();
    this.tabs.addTab("Henvendelser i KS", null, getHenvendelseriKS(), "Henvendelser i KS");
    this.tabs.setMnemonicAt(0, 49);
    this.tabs.addTab("Interne telefoniske henvendelser i KS", null, getInterneTelefoniskehenvendelseriKS(), "Interne telefoniske henvendelser i KS");
    this.tabs.setMnemonicAt(1, 49);
    this.tabs.addTab("Besvarede henvendelser i KS", null, getChartBesvarelsesPct(), "Besvarede henvendelser i KS");
    this.tabs.setMnemonicAt(2, 49);
    this.tabs.addTab("Telefonisk besvarelsesprocent", null, telefoniskBesvarelsesprocent(), "Telefonisk besvarelsesprocent");
    this.tabs.setMnemonicAt(3, 49);
    this.tabs.addTab("Email antal", null, getEmailAntal(), "Email antal");
    this.tabs.setMnemonicAt(4, 49);
    this.tabs.addTab("Email h�ndtering", null, getChartEmailBesvarelsesPct(), "Email h�ndtering");
    this.tabs.setMnemonicAt(5, 49);
    this.tabs.addTab("Svarprocenter p� kanaler", null, getChartEmailData(), "Svarprocenter p� kanaler");
    this.tabs.setMnemonicAt(6, 49);
    this.tabs.addTab("Callbacks", null, getCallbacks(), "Callbacks");
    this.tabs.setMnemonicAt(6, 49);

    return this.tabs;
  } 

  private ChartWithTable5 getCallbacks() {
    this.chartCallbacks = new ChartWithTable5(true, 275, "");
    return this.chartCallbacks;
  }
  private ChartWithTable getHenvendelseriKS() {
    this.chartHenvendelser = new ChartWithTable(true, 275, " ");
    return this.chartHenvendelser;
  }
  private ChartWithTable6 getEmailAntal() {
    this.chartEmailHenvendelser = new ChartWithTable6(true, 275, " ");
    return this.chartEmailHenvendelser;
  }
  private ChartWithTable4 getChartEmailData() {
    this.chartEmailData = new ChartWithTable4(true, 275, "");
    return this.chartEmailData;
  }
  private ChartWithTable3 getInterneTelefoniskehenvendelseriKS() {
    this.chartInterneHenvendelser = new ChartWithTable3(true, 275, "");
    return this.chartInterneHenvendelser;
  }
  private ChartWithTable2 telefoniskBesvarelsesprocent() {
    this.chartBesvarelsesPct25Sek = new ChartWithTable2(false, 275, "");
    return this.chartBesvarelsesPct25Sek;
  }
  private ChartWithTable8 getChartBesvarelsesPct() {
    this.telefoniskBesvarelsesprocent = new ChartWithTable8(false, 275, "");
    return this.telefoniskBesvarelsesprocent;
  }
  private ChartWithTable7 getChartEmailBesvarelsesPct() {
    this.chartEmailBesvarelsesPct = new ChartWithTable7(true, 275, "");
    return this.chartEmailBesvarelsesPct;
  }
}

